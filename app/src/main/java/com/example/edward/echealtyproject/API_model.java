package com.example.edward.echealtyproject;

import android.util.Log;

/**
 * Created by edward on 11/18/2015.
 */
public class API_model{
    /**
     * platform mode:
     * 1=iPad , 2=iPad SSO, 3=Web, 4=Mobile Web, 5=HKU SSO, 6=HKU SSO (Mobile), 7 = HKU Moodle,
     * 8 = iPhone, 9 = iPhone SSO, 10 = Android Tablet; 11 = Android Phone
     */

    final  String DEPLOYMENT_PATH = "http://echealth.iclassu.com/ios/";
    final String BETA_PATH = "http://echealth.iclassu.com/ios/beta/";

    final String TAG = "API_model";
    private String platformid = "11";

    private static volatile API_model instance = null;

    // private constructor suppresses
    private API_model(){
    }

    public static API_model getInstance() {
        // if already inited, no need to get lock everytime
        if (instance == null) {
            synchronized (API_model.class) {
                if (instance == null) {
                    instance = new API_model();
                }
            }
        }
        return instance;
    }

    String disclaimer(){
        return "http://echealth.iclassu.com/privacy/Disclaimer.html";
    }

    /**
     *
     * @param user_name
     * @param password
     * @return
     *
     *  <plist version='1.0'>
    <dict>
    <key>Success</key>
    <string>1000</string>
    <key>MemberID</key>
    <string>2</string>
    <key>MemberType</key>
    <string>2</string>
    <key>GuestLogin</key>
    <string>-1</string>
    <key>Username</key>
    <string>demo</string>
    <key>Password</key>
    <string>demo</string>
    <key>Name</key>
    <string>Demo Account</string>
    <key>Nickname</key>
    <string>Demo</string>
    <key>SchoolID</key>
    <string>1</string>
    <key>Class</key>
    <string>--</string>
    <key>StudentNumber</key>
    <string>--</string>
    <key>ProfilePic</key>
    <string></string>
    <key>Language</key>
    <string>2</string>
    <key>FirstLoad</key>
    <string>1</string>
    </dict>
    </plist>
     *
     */
    String login(String user_name,String password){
        return BETA_PATH+"login.php?username="+user_name+"&password="+password+"&platformid="+platformid;
    }

    /**
     *
     * @param course_id
     * @return
     *
     * <plist version='1.0'>
    <array>
    <dict>
    <key>SurveyID</key>
    <string>1000</string>
    <key>Name</key>
    <string>What to do</string>
    <key>Name_ZH-CN</key>
    <string>指引</string>
    <key>IconPath</key>
    <string></string>
    <key>IconPath_ZH-CN</key>
    <string>Level One_Whattodo-zhcn.png</string>
    </dict>
    <dict>
    <key>SurveyID</key>
    <string>10</string>
    <key>Name</key>
    <string>My family</string>
    <key>Name_ZH-CN</key>
    <string>我的家庭</string>
    <key>IconPath</key>
    <string></string>
    <key>IconPath_ZH-CN</key>
    <string>My_family-zhcn.png</string>
    </dict>
    <dict>
    <key>SurveyID</key>
    <string>11</string>
    <key>Name</key>
    <string>Pattern of diseases in the family suggestive of genetic influences</string>
    <key>Name_ZH-CN</key>
    <string>家中疾病分布呈遗传性</string>
    <key>IconPath</key>
    <string></string>
    <key>IconPath_ZH-CN</key>
    <string>Pattern_of_disease_in_the_family_suggestive_of_genetic_influences-zhcn.png</string>
    </dict>
    <dict>
    <key>SurveyID</key>
    <string>12</string>
    <key>Name</key>
    <string>Family history of probable hereditary conditions</string>
    <key>Name_ZH-CN</key>
    <string>家中出现疑似遗传病例</string>
    <key>IconPath</key>
    <string></string>
    <key>IconPath_ZH-CN</key>
    <string>Family_history_of_probable_hereditary_conditions-zhcn.png</string>
    </dict>
    <dict>
    <key>SurveyID</key>
    <string>1001</string>
    <key>Name</key>
    <string>Summary</string>
    <key>Name_ZH-CN</key>
    <string>摘要</string>
    <key>IconPath</key>
    <string></string>
    <key>IconPath_ZH-CN</key>
    <string>summary-zhcn.png</string>
    </dict>
    <dict>
    <key>LastEdit</key>
    <string>2014-12-18 15:37:20</string>
    </dict>
    </array>
    </plist>

     *
     */
    String icons(String course_id){
        if (Integer.parseInt(course_id)<10){
            //survey
            return BETA_PATH+"survey_icons.php?course_id="+course_id;
        }else {
            //record
            return BETA_PATH+"record_icons.php?course_id="+course_id;
        }
    }

    /**
     *
     * @param course_id
     * @return
     *
     * <plist version='1.0'>
    <dict>
    <key>CourseID</key>
    <string>1</string>
    <key>Name</key>
    <string>Demographics</string>
    <key>Name_ZH-CN</key>
    <string>基本个人资料</string>
    <key>Description</key>
    <string>Demographics, such as age and place of residence, allow medical practitioners to stratify your risk profile.

    As in every module, please kindly select answers within a comfortable boundary of privacy. All contents are solely designed to facilitate an efficient medical consultation.
    </string>
    <key>Description_ZH-CN</key>
    <string>个人资料，如年龄及居住地点，能让医护人员初步估计您所面对的健康风险。

    一如其他模组，请在符合私隐范围内填上选项。所有内容均以会诊所需而设计。</string>
    <key>LastEdit</key>
    <string>2014-12-18 15:33:02</string>
    </dict>
    </plist>
     */

    //beta server address:ios/beta/beta/

    String survey_and_record_info(String course_id){
        if (Integer.parseInt(course_id)<10){
            return BETA_PATH+"survey_info.php?course_id="+course_id;
        }else {
            return BETA_PATH+"record_info.php?course_id="+course_id;
        }
    }

    /**
     *
     * @param member_id
     * @param survey_id
     * @return
     * http://echealth.iclassu.com/ios/beta/survey_questions.php?survey_id=1&member_id=2
     */
    String survey_id_questions(String member_id,String survey_id){
        return BETA_PATH+"survey_questions.php?survey_id="+survey_id+"&member_id="+member_id;
    }

    String record_id_questions(String member_id,String record_id){
        return BETA_PATH+"record_questions.php?record_id="+record_id+"&member_id="+member_id;
    }

    /**
     *
     * @param member_id
     * @param survey_item_id
     * @return
     */
    public String survey_item_id_questions(String member_id,String survey_item_id){
        return BETA_PATH+"survey_questions.php?survey_item_id="+survey_item_id+"&member_id="+member_id;
    }

    String record_level3_questions(String member_id,String level2_record_id){
        Log.i(TAG, BETA_PATH+"record_questions.php?level2_record_id="+level2_record_id+"&member_id="+member_id);
        return BETA_PATH+"record_questions.php?level2_record_id="+level2_record_id+"&member_id="+member_id;
    }

    String record_item_id_questions(String member_id,String record_item_id){
        return BETA_PATH+"survey_questions.php?record_item_id="+record_item_id+"&member_id="+member_id;
    }

    /**
     *
     * @param member_id
     * @param survey_id
     * @return
     * http://echealth.iclassu.com/ios/beta/survey_answers.php?survey_id=1&member_id=2
     */
    String survey_answers(String member_id,String survey_id){
        return BETA_PATH+"survey_answers.php?survey_id="+survey_id+"&member_id="+member_id;
    }

    String survey_item_answers(String member_id,String survey_item_id){
        return BETA_PATH+"survey_answers.php?survey_item_id="+survey_item_id+"&member_id="+member_id;
    }

    public String survey_submit_single(){
        return BETA_PATH+"submit_survey_single.php";
    }

    String record_answers_with_recordid(String member_id,String record_id,String attempt){
        return BETA_PATH+"record_answers.php?record_id="+record_id+"&member_id="+member_id+"&attempt="+attempt;
    }

    String record_answers_with_level2_record_id(String member_id,String level2_record_id,String attempt){
        return BETA_PATH+"record_answers.php?level2_record_id="+level2_record_id+"&member_id="+member_id+"&attempt="+attempt;
    }

    String record_item_answers(String member_id,String record_id,String attempt){
        return BETA_PATH+"record_answers.php?record_id="+record_id+"&member_id="+member_id+"&attempt="+attempt;
    }

    String record_submit_single(){
        return BETA_PATH+"submit_record_single.php";
    }

    String survey_summary(String member_id,String course_id){
        return BETA_PATH+"survey_summary.php?course_id="+course_id+"&member_id="+member_id;
    }

    String survey_delete_answer(){
        return BETA_PATH+"survey_delete_answer.php";
    }

    String record_summary(String member_id,String record_id){
        return BETA_PATH+"record_summary.php?record_id="+record_id+"&member_id="+member_id;
    }

    String record_delete_answer(){
        //POST PARAMS : String member_id,String record_id,String attempt
        return BETA_PATH+"record_delete_answer.php";
    }

    String record_attempt(String member_id,String record_id){
        return  BETA_PATH+"record_attempt.php?record_id="+record_id+"&member_id="+member_id;
    }
}
