package com.example.edward.echealtyproject;

/**
 * Created by edward on 1/22/2016.
 */
public interface QuestionActivityInterface {
    void post_single_survey_answer(String answer_string,String item_id,OnPostAnswerInterface onPostAnswerInterface);
    void post_single_record_answer();
}