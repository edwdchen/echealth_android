package com.example.edward.echealtyproject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.edward.echealtyproject.plist.Plist;
import com.example.edward.echealtyproject.plist.XmlParseException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by edward on 11/19/2015.
 */
public class ModuleActvity extends AppCompatActivity {
    final String TAG = "ModuleActvity";
    GridView gridView;
    BaseAdapter gridview_adapter;
    RequestQueue mQueue;
    String course_id = "";
    int MODE;
    final int RECORD_MODE = 222, SURVEY_MODE = 333;
    ArrayList<HashMap<String, String>> activity_list;
    ProgressDialog progressDialog;
    int spacing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.module_layout);

        progressDialog = new ProgressDialog(ModuleActvity.this);
        progressDialog.show();

        mQueue = Volley.newRequestQueue(ModuleActvity.this);
        course_id = String.valueOf(getIntent().getIntExtra("course_id", 0));
        if (getIntent().getIntExtra("course_id", 0) < 10) {
            MODE = SURVEY_MODE;
        } else {
            MODE = RECORD_MODE;
        }
        Button back = (Button) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button finish = (Button) findViewById(R.id.finish_button);
        finish.setVisibility(View.INVISIBLE);

        final TextView title = (TextView) findViewById(R.id.title_textview);
        title.setText(getIntent().getStringExtra("course_name"));

        gridView = (GridView) findViewById(R.id.gridview);

        int screenWidth = ((Activity) ModuleActvity.this).getWindowManager()
                .getDefaultDisplay().getWidth();

        spacing = screenWidth / 32;
        gridView.setVerticalSpacing(spacing);
        gridView.setHorizontalSpacing(spacing);
        gridView.setPadding(spacing, spacing, spacing, spacing);

        if (getIntent().getIntExtra("course_id", 0) == -1) {
            final int[] array = new int[]{R.mipmap.introduction_1introduction_zhcn, R.mipmap.introduction_2navigation_zhcn};
            gridview_adapter = new BaseAdapter() {
                @Override
                public int getCount() {
                    return 2;
                }

                @Override
                public Object getItem(int position) {
                    return array[position];
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(final int position, View convertView, ViewGroup parent) {
                    ImageView imageView = new ImageView(ModuleActvity.this);
                    imageView.setImageResource((int) getItem(position));
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            switch (position) {
                                case 0:
                                    Intent intro_intent = new Intent(ModuleActvity.this, ThanksActivity.class);
                                    intro_intent.putExtra("type", 1);
                                    startActivity(intro_intent);
                                    break;
                                case 1:
                                    Intent navi_intent = new Intent(ModuleActvity.this, ThanksActivity.class);
                                    navi_intent.putExtra("type", 2);
                                    startActivity(navi_intent);
                                    break;
                            }
                        }
                    });
                    return imageView;
                }
            };
            gridView.setAdapter(gridview_adapter);
            progressDialog.dismiss();

        } else if (getIntent().getIntExtra("course_id", 0) == 0) {//summary
            final int[] array = new int[]{R.mipmap.summaryexecutive_zhcn_hd, R.mipmap.summarygrand_zhcn_hd};
            gridview_adapter = new BaseAdapter() {
                @Override
                public int getCount() {
                    return array.length;
                }

                @Override
                public Object getItem(int position) {
                    return array[position];
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(final int position, View convertView, ViewGroup parent) {
                    ImageView imageView = new ImageView(ModuleActvity.this);
                    imageView.setImageResource((int) getItem(position));
                    return imageView;
                }
            };
            gridView.setAdapter(gridview_adapter);
            progressDialog.dismiss();
        } else {

            gridview_adapter = new BaseAdapter() {

                //-1 is  because the last index is used for marking the last edit time
                @Override
                public int getCount() {
                    return activity_list.size() - 1;
                }

                @Override
                public HashMap<String, String> getItem(int position) {
                    return activity_list.get(position);
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    ImageView imageView = new ImageView(ModuleActvity.this);
                    if (MODE == SURVEY_MODE) {
                        switch (Integer.parseInt(activity_list.get(position).get("SurveyID"))) {
                            case 1000://What to do
                                imageView.setImageResource(R.mipmap.one_whattodo_zhcn);
                                imageView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(ModuleActvity.this, InfoActivity.class);
                                        intent.putExtra("course_id", course_id);
                                        startActivity(intent);
                                    }
                                });
                                break;
                            case 1001://Summary
                                imageView.setImageResource(R.mipmap.summary_zhcn);
                                imageView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(ModuleActvity.this, SummaryActivity.class);
                                        intent.putExtra("course_id", course_id);
                                        intent.putExtra("title", title.getText().toString());
                                        intent.putExtra("MODE", Global.SURVERY_MODE);
                                        startActivity(intent);
                                    }
                                });
                                break;
                            case 1://birth_and_marriage
                                imageView.setImageResource(R.mipmap.birth_and_marriage_zhcn);
                                break;
                            case 2://career_development
                                imageView.setImageResource(R.mipmap.career_development_zhcn);
                                break;
                            case 3://religious_belief
                                imageView.setImageResource(R.mipmap.religious_belief_zhcn);
                                break;
                            case 4://Childhood immunisation
                                imageView.setImageResource(R.mipmap.childhood_immunisation_zhcn);
                                break;
                            case 5://Adult immunisation
                                imageView.setImageResource(R.mipmap.adult_immunisation_zhcn);
                                break;
                            case 6://Adult catch-up immunisation
                                imageView.setImageResource(R.mipmap.adult_catch_up_immunisation_zhcn);
                                break;
                            case 7://Travel vaccinations and prophylaxis
                                imageView.setImageResource(R.mipmap.travel_vaccination_and_prophylaxis_zhcn);
                                break;
                            case 8://Immunisation for specific occupations
                                imageView.setImageResource(R.mipmap.immunisation_for_specific_occupations_zhcn);
                                break;
                            case 9://sex
                                imageView.setImageResource(R.mipmap.sexually_transmitted_disease_screening_for_high_risk_individuals_zhcn);
                                break;
                            case 10://My family
                                imageView.setImageResource(R.mipmap.my_family_zhcn);
                                break;
                            case 11://Pattern of diseases in the family suggestive of genetic influences
                                imageView.setImageResource(R.mipmap.pattern_of_disease_in_the_family_suggestive_of_genetic_influences_zhcn);
                                break;
                            case 12://Family history of probable hereditary conditions
                                imageView.setImageResource(R.mipmap.family_history_of_probable_hereditary_conditions_zhcn);
                                break;
                            case 13://Employment
                                imageView.setImageResource(R.mipmap.employment_zhcn);
                                break;
                            case 14://work hazard
                                imageView.setImageResource(R.mipmap.work_hazards_zhcn);
                                break;
                            case 15://Employment
                                imageView.setImageResource(R.mipmap.living_environment_zhcn);
                                break;
                            case 16://Transport safety
                                imageView.setImageResource(R.mipmap.transport_safety_zhcn);
                                break;
                            case 17://General risk factors for cancer
                                imageView.setImageResource(R.mipmap.general_risk_factors_for_cancer_zhcn);
                                break;
                            case 18://Risk factors for specific cancers
                                imageView.setImageResource(R.mipmap.risk_factors_for_specific_cancers_zhcn);
                                break;
                            case 19://Common screening tests, proactive preventive measures
                                imageView.setImageResource(R.mipmap.cancerprevention_proactiveprevention_zhcn);
                                break;
                            case 20://allergy_symptoms_awareness_zhcn
                                imageView.setImageResource(R.mipmap.allergy_symptoms_awareness_zhcn);
                                break;
                            case 21://Documented acute allergic reactions
                                imageView.setImageResource(R.mipmap.documented_acute_allergic_reactions_zhcn);
                                break;
                            case 22://Allergen(s) you think are associated with your symptoms
                                imageView.setImageResource(R.mipmap.possible_allergens_associated_with_your_symptoms_zhcn);
                                break;
                            case 23://allergy_tests_zhcn
                                imageView.setImageResource(R.mipmap.allergy_tests_zhcn);
                                break;
                            case 24://Development and personality
                                imageView.setImageResource(R.mipmap.development_and_personality_zhcn);
                                break;
                            case 25://Stress_and_coping
                                imageView.setImageResource(R.mipmap.stress_and_coping_zhcn);
                                break;
                            case 26://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.access_and_compiance_in_healthcare_services_zhcn);
                                break;
                            case 27://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.daily_physical_activity_zhcn);
                                break;
                            case 28://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.blood_pressure_pulse_and_body_measurements_zhcn);
                                break;
                            case 29://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.fitness_level_zhcn);
                                break;
                            case 30://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.exercise_and_non_competitive_sports_zhcn);
                                break;
                            case 31://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.competitive_sports_zhcn);
                                break;
                            case 32://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.over_and_under_nutrition_zhcn);
                                break;
                            case 33://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.quality_of_food_and_beverages_intake_zhcn);
                                break;
                            case 34://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.herbs_and_supplement_use_zhcn);
                                break;
                            case 35://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.tobacco_use_zhcn);
                                break;
                            case 36://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.alcohol_use_zhcn);
                                break;
                            case 37://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.addictive_drug_use_zhcn);
                                break;
                            case 38://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.personal_hygiene_zhcn);
                                break;
                            case 39://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.communication_and_cognition_zhcn);
                                break;
                            case 40://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.interests_and_hobbies_zhcn);
                                break;
                            case 41://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.sexual_health_and_family_planning_zhcn);
                                break;
                            case 42://Access_and_compiance_in_healthcare_services
                                imageView.setImageResource(R.mipmap.sleep_quality_zhcn);
                                break;

                        }
                    } else if (MODE == RECORD_MODE) {
                        if (activity_list.get(position) != null) {

                            switch (Integer.parseInt(activity_list.get(position).get("RecordID"))) {
                                case 1000://What to do
                                    imageView.setImageResource(R.mipmap.one_whattodo_zhcn);
                                    imageView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(ModuleActvity.this, InfoActivity.class);
                                            intent.putExtra("course_id", course_id);
                                            startActivity(intent);
                                        }
                                    });
                                    break;
                                case 1001://Summary
                                    imageView.setImageResource(R.mipmap.summary_zhcn);
                                    imageView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(ModuleActvity.this, SummaryActivity.class);
                                            intent.putExtra("course_id", course_id);
                                            ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) activity_list.clone();
                                            System.out.println("list:"+list);
                                            if (course_id.equals("10")) {

                                                list.remove(1);
                                                list.remove(1);

                                                intent.putExtra("course_info", list);
                                                System.out.println("list:" + list);
                                            } else {
                                                intent.putExtra("course_info", activity_list);
                                            }

                                            intent.putExtra("title", title.getText().toString());
                                            intent.putExtra("MODE", Global.RECORD_MODE);
                                            startActivity(intent);
                                        }
                                    });

                                    break;
                                case 1://全身症状
                                    imageView.setImageResource(R.mipmap.constitutional_symptoms_zhcn_hd);
                                    break;
                                case 2://Psychological_symptoms
                                    imageView.setImageResource(R.mipmap.psychological_symptoms_zhcn_hd);
                                    break;
                                case 3://religious_belief
                                    imageView.setImageResource(R.mipmap.skin_and_nail_symptoms_zhcn_hd);
                                    break;
                                case 4://Childhood immunisation
                                    imageView.setImageResource(R.mipmap.symptoms_related_to_gait_and_body_movement_zhcn_hd);
                                    break;
                                case 5://
                                    imageView.setImageResource(R.mipmap.sleep_related_symptoms_zhcn_hd);
                                    break;
                                case 6://Adult catch-up immunisation
                                    imageView.setImageResource(R.mipmap.eating_and_drinking_related_symptoms_zhcn_hd);
                                    break;
                                case 7://Travel vaccinations and prophylaxis
                                    imageView.setImageResource(R.mipmap.speech_related_symptoms_zhcn_hd);
                                    break;
                                case 8://Immunisation for specific occupations
                                    imageView.setImageResource(R.mipmap.defecation_related_symptoms_zhcn_hd);
                                    break;
                                case 9://sex
                                    imageView.setImageResource(R.mipmap.micturiation_related_symptoms_zhcn_hd);
                                    break;
                                case 10://My family
                                    imageView.setImageResource(R.mipmap.head_and_neck_symptoms_zhcn_hd);
                                    break;
                                case 11://Pattern of diseases in the family suggestive of genetic influences
                                    imageView.setImageResource(R.mipmap.chest_and_breast_symptoms_zhcn_hd);
                                    break;
                                case 12://Family history of probable hereditary conditions
                                    imageView.setImageResource(R.mipmap.adominal_and_flank_symptoms_zhcn_hd);
                                    break;
                                case 13://Employment
                                    imageView.setImageResource(R.mipmap.back_and_perianal_symptoms_zhcn_hd);
                                    break;
                                case 14://work hazard
                                    imageView.setImageResource(R.mipmap.upper_limb_symptoms_zhcn_hd);
                                    break;
                                case 15://Employment
                                    imageView.setImageResource(R.mipmap.lower_limb_symptoms_zhcn_hd);
                                    break;
                                case 16://Transport safety
                                    imageView.setImageResource(R.mipmap.sympotoms_related_to_sexual_organs_zhcn_hd);
                                    break;
                                case 17://General risk factors for cancer
                                    imageView.setImageResource(R.mipmap.chronic_diseases_zhcn_hd);
                                    break;
                                case 18://Risk factors for specific cancers
                                    imageView.setImageResource(R.mipmap.cancers_zhcn_hd);
                                    break;
                                case 19://Common screening tests, proactive preventive measures
                                    imageView.setImageResource(R.mipmap.physical_overuse_orthopaedic_injuries_zhcn_hd);
                                    break;
                                case 20://allergy_symptoms_awareness_zhcn
                                    imageView.setImageResource(R.mipmap.physical_injury_to_organs_zhcn_hd);
                                    break;
                                case 21://Documented acute allergic reactions
                                    imageView.setImageResource(R.mipmap.illnesses_arising_from_environmental_elements_zhcn_hd);
                                    break;
                                case 22://Allergen(s) you think are associated with your symptoms
                                    imageView.setImageResource(R.mipmap.bites_and_stings_zhcn_hd);
                                    break;
                                case 23://allergy_tests_zhcn
                                    imageView.setImageResource(R.mipmap.treatment_related_and_self_inflicted_injuries_zhcn_hd);
                                    break;
                                case 24://Development and personality
                                    imageView.setImageResource(R.mipmap.surgeries_zhcn_hd);
                                    break;
                                case 25://Stress_and_coping
                                    imageView.setImageResource(R.mipmap.blood_transfusions_zhcn_hd);
                                    break;
                                case 26://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.skin_diseases_zhcn_hd);
                                    break;
                                case 27://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.orthopaedic_discorders_non_traumatic_non_rheumatic_zhcn_hd);
                                    break;
                                case 28://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.eye_disorders_zhcn_hd);
                                    break;
                                case 29://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.ear_nose_throat_and_neck_conditions_zhcn_hd);
                                    break;
                                case 30://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.neurological_diseases_zhcn_hd);
                                    break;
                                case 31://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.psychiatric_dieases_zhcn_hd);
                                    break;
                                case 32://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.cardiovascular_dieases_zhcn_hd);
                                    break;
                                case 33://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.respiratory_diseases_zhcn_hd);
                                    break;
                                case 34://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.dental_and_faciomaxillary_disorders_zhcn_hd);
                                    break;
                                case 35://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.gastrointestinal_diseases_zhcn_hd);
                                    break;
                                case 36://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.hepatobiliary_pancreatic_diseases_zhcn_hd);
                                    break;
                                case 37://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.renal_and_urologic_diseases_zhcn_hd);
                                    break;
                                case 38://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.male_genital_tract_disorders_zhcn_hd);
                                    break;
                                case 39://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.gynaecological_disorders_zhcn_hd);
                                    break;
                                case 40://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.benign_breast_disorders_zhcn_hd);
                                    break;
                                case 41://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.endocrine_and_metabolic_diseases_zhcn_hd);
                                    break;
                                case 42://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.haematological_diseases_zhcn_hd);
                                    break;
                                case 43://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.infectious_diseases_zhcn_hd);
                                    break;
                                case 44://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.rheumatological_diseases_zhcn_hd);
                                    break;
                                case 45://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.allergic_reaction_to_drugs_zhcn_hd);
                                    break;
                                case 46://Access_and_compiance_in_healthcare_services
                                    imageView.setImageResource(R.mipmap.drug_intolerance_zhcn_hd);
                                    break;
                                case 47:
                                    imageView.setImageResource(R.mipmap.conditions_affecting_prescribing_method_zhcn_hd);
                                    break;
                                case 48:
                                    imageView.setImageResource(R.mipmap.healthcare_institutions_zhcn_hd);
                                    break;
                                case 49:
                                    imageView.setImageResource(R.mipmap.medical_practitioners_zhcn_hd);
                                    break;
                                case 50:
                                    imageView.setImageResource(R.mipmap.paramedical_personnel_zhcn_hd);
                                    break;
                                case 51:
                                    imageView.setImageResource(R.mipmap.traditional_chinese_medicine_practitioners_zhcn_hd);
                                    break;
                                case 52:
                                    imageView.setImageResource(R.mipmap.health_insurance_zhcn_hd);
                                    break;
                                case 53:
                                    imageView.setImageResource(R.mipmap.tablets_for_oral_intake_zhcn_hd);
                                    break;
                                case 54:
                                    imageView.setImageResource(R.mipmap.liquid_for_oral_intake_zhcn_hd);

                                    break;
                                case 55:
                                    imageView.setImageResource(R.mipmap.mouth_wash_lozenges_or_oral_gel_zhcn_hd);
                                    break;
                                case 56:
                                    imageView.setImageResource(R.mipmap.suppositories_zhcn_hd);

                                    break;
                                case 57:
                                    imageView.setImageResource(R.mipmap.cream_ointment_or_lotion_zhcn_hd);

                                    break;
                                case 58:
                                    imageView.setImageResource(R.mipmap.skin_patch_zhcn_hd);

                                    break;
                                case 59:
                                    imageView.setImageResource(R.mipmap.eye_drops_ointment_or_gel__zhcn_hd);

                                    break;
                                case 60:
                                    imageView.setImageResource(R.mipmap.ear_drops_or_ointment_zhcn_hd);
                                    break;
                                case 61:
                                    imageView.setImageResource(R.mipmap.nasal_spray_or_powder_zhcn_hd);
                                    break;
                                case 62:
                                    imageView.setImageResource(R.mipmap.inhalation_therapy_zhcn_hd);
                                    break;
                                case 63:
                                    imageView.setImageResource(R.mipmap.injection_self_administered_zhcn_hd);
                                    break;
                                case 64:
                                    imageView.setImageResource(R.mipmap.injection_or_deppot_by_healthcare_workers_zhcn_hd);
                                    break;
                                case 65:
                                    imageView.setImageResource(R.mipmap.ear_drops_or_ointment_zhcn_hd);
                                    break;

                            }
                        }
                    }

                    int screenWidth = ((Activity) ModuleActvity.this).getWindowManager()
                            .getDefaultDisplay().getWidth();

                    imageView.setLayoutParams(new GridView.LayoutParams(GridView.LayoutParams.MATCH_PARENT, (int) ((float) screenWidth / 3.5)));

                    return imageView;
                }
            };

            StringRequest stringRequest = new StringRequest(API_model.getInstance().icons(course_id),
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i(TAG, API_model.getInstance().icons(course_id));
                            Log.d(TAG, response);
                            try {
                                activity_list = (ArrayList<HashMap<String, String>>) Plist.objectFromXml(response);

                                if (course_id.equals("10")) {
                                    activity_list.add(1, null);
                                    activity_list.add(1, null);
                                }

                                gridView.setAdapter(gridview_adapter);
                                progressDialog.dismiss();
                            } catch (XmlParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, error.getMessage(), error);
                }
            });

            mQueue.add(stringRequest);

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent();
                    if (MODE == SURVEY_MODE) {
                        intent.putExtra("survey_mode?", true);
                        intent.setClass(ModuleActvity.this, SurveyQuestionActivity.class);
                        intent.putExtra("SurveyID", activity_list.get(position).get("SurveyID"));
                        intent.putExtra("title", activity_list.get(position).get("Name_ZH-CN"));
                    } else {
                        intent.putExtra("survey_mode?", false);
                        intent.setClass(ModuleActvity.this, SurveyQuestionActivity.class);
                        intent.putExtra("RecordID", activity_list.get(position).get("RecordID"));
                        intent.putExtra("title", activity_list.get(position).get("Name_ZH-CN"));
                    }
                    startActivity(intent);
                }
            });
        }
    }

}
