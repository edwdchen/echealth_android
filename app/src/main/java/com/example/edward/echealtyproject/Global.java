package com.example.edward.echealtyproject;

import android.text.Html;

import java.io.UnsupportedEncodingException;

/**
 * Created by edward on 11/18/2015.
 */
public class Global {

    public static final int SURVERY_MODE = 444;
    public static final int RECORD_MODE = 555;

    private static volatile Global instance = null;

    // private constructor suppresses
    private Global() {
    }

    public static Global getInstance() {
        // if already inited, no need to get lock everytime
        if (instance == null) {
            synchronized (API_model.class) {
                if (instance == null) {
                    instance = new Global();
                }
            }
        }
        return instance;
    }

    public String getCurrent_course_id() {
        return current_course_id;
    }

    public void setCurrent_course_id(String current_course_id) {
        this.current_course_id = current_course_id;
    }

    private String current_course_id;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    private String user_name;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String password;

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    private String member_id;

    String getUTFString(String s) {
        //server bug fixed, this method should not be used anymore
        String cn_content = "";
        try {
            cn_content = new String(s.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Html.fromHtml(cn_content).toString();
    }


}
