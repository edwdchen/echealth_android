package com.example.edward.echealtyproject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.edward.echealtyproject.plist.PlistHandler;
import com.example.edward.echealtyproject.question_view.ChecklistView;
import com.example.edward.echealtyproject.question_view.DatePickerView;
import com.example.edward.echealtyproject.question_view.DropdownMenuView;
import com.example.edward.echealtyproject.question_view.PickerView;
import com.example.edward.echealtyproject.question_view.SingleCheckBoxView;
import com.example.edward.echealtyproject.question_view.ToggleView;

import org.w3c.dom.Attr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by edward on 12/10/2015.
 */
public class RecordQuestionActivity extends PostAnswerActivity {
    /**
     * there are 3 conditions
     * 1:level2
     * 2:level3 without attempt
     * 3:level3 with attempt (in this condition, we have to get submitted answers from server first)
     *
     *
     *
     * getIntent() params:
     * title    String
     * level3mode?  boolean
     * RecordID
     * RecordItemID
     */
    ArrayList<HashMap<String, Object>> question_list;
    ArrayList<HashMap<String, Object>> answer_list;
    ListView listView;
    BaseAdapter listview_adapter;

    boolean first_initiate = true;
    String RecordID;
    String RecordItemID;
    String attempt;

    Button back;
    Button finish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_list_layout);

        if (null != getIntent().getStringExtra("RecordID")) {
            RecordID = getIntent().getStringExtra("RecordID");
        }

        if (null != getIntent().getStringExtra("RecordItemID")) {
            RecordItemID = getIntent().getStringExtra("RecordItemID");
        }

        if (null != getIntent().getStringExtra("Attempt")){
            attempt = getIntent().getStringExtra("Attempt");
        }

        back = (Button) findViewById(R.id.back_button);
        back.setText("完成");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        finish = (Button) findViewById(R.id.finish_button);
        finish.setVisibility(View.INVISIBLE);

        TextView title = (TextView) findViewById(R.id.title_textview);
        if (getIntent().getStringExtra("title") == null) {
            //level3
            title.setText("");
        } else {
            title.setText(getIntent().getStringExtra("title"));
        }

        listView = (ListView) findViewById(R.id.listview);

        listview_adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return question_list.size() - 1;
            }

            @Override
            public HashMap<String, ?> getItem(int position) {
                return question_list.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View listview_item = null;
                TextView item_name;
                switch (Integer.parseInt((String) question_list.get(position).get("QuestionType"))) {
                    case 0://header
                        listview_item = View.inflate(RecordQuestionActivity.this, R.layout.survey_question_header_item, null);
                        item_name = (TextView) listview_item.findViewById(R.id.item_name);
                        item_name.setText((String) question_list.get(position).get("Name_ZH-CN"));
                        break;

                    case 1://PICKER
                        listview_item = new PickerView(RecordQuestionActivity.this, question_list.get(position), null, Global.RECORD_MODE, position).create_question_listview_view();
                        break;

                    case 2://single checkbox
                        listview_item = new SingleCheckBoxView(RecordQuestionActivity.this, question_list.get(position), null, Global.RECORD_MODE, position).create_question_listview_view();
                        break;

                    case 3://toggle
                        listview_item = new ToggleView(RecordQuestionActivity.this, question_list.get(position), null, Global.RECORD_MODE, position).create_question_listview_view();
                        break;

                    case 4://text
                        listview_item = View.inflate(RecordQuestionActivity.this, R.layout.survey_question_text_item, null);
                        item_name = (TextView) listview_item.findViewById(R.id.item_name);
                        item_name.setText((String) question_list.get(position).get("Content_ZH-CN"));
                        TextView label1 = (TextView) listview_item.findViewById(R.id.text_label1);
                        TextView label2 = (TextView) listview_item.findViewById(R.id.text_label2);
                        label1.setText((String) question_list.get(position).get("Label_ZH-CN"));
                        label2.setText((String) question_list.get(position).get("Label_ZH-CN"));

                        final EditText editText1, editText2;
                        editText1 = (EditText) listview_item.findViewById(R.id.edittext1);
                        editText2 = (EditText) listview_item.findViewById(R.id.edittext2);
                        //record
                        if ((question_list.get(position).get("Type")).equals("2")) {
                            editText1.setHint("中文");
                            editText2.setHint("英文或拼音");
                        } else {
                            editText2.setVisibility(View.GONE);
                        }

                        break;

                    case 5://SliderQuestion

                        listview_item = View.inflate(RecordQuestionActivity.this, R.layout.survey_question_sliding_item, null);
                        item_name = (TextView) listview_item.findViewById(R.id.item_name);
                        item_name.setText((String) question_list.get(position).get("Content_ZH-CN"));
                        TextView slider_description = (TextView) listview_item.findViewById(R.id.description);

                        if (null != question_list.get(position).get("Description_ZH-CN") || ((String) question_list.get(position).get("Description_ZH-CN")).length() > 0) {

                            slider_description.setText((String) question_list.get(position).get("Description_ZH-CN"));
                        }
                        final TextView slider_level3_btn = (TextView) listview_item.findViewById(R.id.level3_btn);

                        break;

                    case 6://dropdown menu
//                        listview_item = new DropdownMenuView(RecordQuestionActivity.this, question_list.get(position), null, false, RecordQuestionActivity.this, position, (ArrayList<String>) temp_answer_array[position]).getInstance();
                        break;

                    case 7://date picker
                        listview_item = new DatePickerView(RecordQuestionActivity.this, false, position).create_question_listview_view();
                        break;

                    case 9://check list
                        listview_item = new ChecklistView(RecordQuestionActivity.this, false, position).getInstance();
                        break;
                }
                return listview_item;
            }
        };

        //findviewbyid and adapter are ready

        new Thread(new Runnable() {
            @Override
            public void run() {

                if (first_initiate) {
                    question_list = PlistHandler.readURLArray(API_model.getInstance().record_level3_questions(Global.getInstance().getMember_id(), RecordID));

                    if (null != attempt){
                        answer_list = PlistHandler.readURLArray(API_model.getInstance().record_answers_with_recordid(Global.getInstance().getMember_id(), RecordID,attempt));
                    }else {
                        for (int i =0;i<question_list.size();i++){
                            answer_list.add(null);
                        }
                    }
                }else {
                    //load data again and overwrite early data

                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listView.setAdapter(listview_adapter);
                    }
                });

            }
        }).start();
    }

    @Override
    public HashMap<String, Object> get_question(int pos) {
        return question_list.get(pos);
    }

    @Override
    public HashMap<String, Object> get_answer(int pos) {
        return answer_list.get(pos);
    }
}
