package com.example.edward.echealtyproject;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by edward on 11/23/2015.
 */
public class SingleChoiceAdapter extends BaseAdapter{

    public int getSelect_item_position() {
        return select_item_position;
    }

    public void setSelect_item_position(int select_item_position) {
        this.select_item_position = select_item_position;
    }

    private int select_item_position = -1;
    ArrayList<HashMap<String,String>> choice_list;
    Context context;
    public SingleChoiceAdapter(Context context,ArrayList<HashMap<String,String>> choice_list) {
        this.choice_list = choice_list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return choice_list.size();
    }

    @Override
    public HashMap<String,String> getItem(int position) {
        return choice_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = View.inflate(context,R.layout.choice_item,null);
        ImageView choice_button = (ImageView) itemView.findViewById(R.id.choice_button);
        TextView choice_text = (TextView) itemView.findViewById(R.id.choice_text);
        choice_text.setText(choice_list.get(position).get("Content_ZH-CN"));
        if (select_item_position==position){
            choice_button.setImageResource(R.mipmap.radio_button);
        }else {
            choice_button.setImageResource(R.mipmap.radio_button_empty);
        }
        return itemView;
    }
}
