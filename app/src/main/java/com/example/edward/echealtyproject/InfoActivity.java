package com.example.edward.echealtyproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.edward.echealtyproject.plist.Plist;
import com.example.edward.echealtyproject.plist.PlistHandler;
import com.example.edward.echealtyproject.plist.XmlParseException;

import java.util.HashMap;

/**
 * Created by edward on 11/19/2015.
 */
public class InfoActivity extends AppCompatActivity {
    String course_id;
    TextView title;
    TextView content;
    HashMap<String, String> info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_layout);

        course_id = getIntent().getStringExtra("course_id");

        Button back = (Button) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button finish = (Button) findViewById(R.id.finish_button);
        finish.setVisibility(View.INVISIBLE);

        title = (TextView) findViewById(R.id.title_textview);
        content = (TextView) findViewById(R.id.info_content);
        content.setMovementMethod(new ScrollingMovementMethod());

        RequestQueue mQueue = Volley.newRequestQueue(InfoActivity.this);

        StringRequest stringRequest = new StringRequest(API_model.getInstance().survey_and_record_info(course_id),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            info = (HashMap<String, String>) Plist.objectFromXml(response);
                        } catch (XmlParseException e) {
                            e.printStackTrace();
                        }

                        title.setText(info.get("Name_ZH-CN"));
                        content.setText(info.get("Description_ZH-CN"));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        mQueue.add(stringRequest);
    }
}
