package com.example.edward.echealtyproject;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by edward on 12/22/2015.
 */
public class ActivityUtil {
    private static volatile ActivityUtil instance = null;

    private ActivityUtil() {
    }
    public static ActivityUtil getInstance() {
        // if already inited, no need to get lock everytime
        if (instance == null) {
            synchronized (ActivityUtil.class) {
                if (instance == null) {
                    instance = new ActivityUtil();
                }
            }
        }
        return instance;
    }

    public void enter_record_level3(Context context,ArrayList<HashMap<String, Object>> level3_list,String record_level3_title,String item_id,String attempt){
        Intent intent = new Intent(context, SurveyQuestionActivity.class);
        intent.putExtra("level3", level3_list);
        intent.putExtra("title", record_level3_title);
        intent.putExtra("level2_record_item_id", item_id);//pass selected level2 item id to next level
        intent.putExtra("survey_mode?", false);
        intent.putExtra("Attempt",attempt);
        context.startActivity(intent);
    }
}
