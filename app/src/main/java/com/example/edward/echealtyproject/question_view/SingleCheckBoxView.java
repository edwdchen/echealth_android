package com.example.edward.echealtyproject.question_view;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.edward.echealtyproject.API_model;
import com.example.edward.echealtyproject.Global;
import com.example.edward.echealtyproject.PostAnswerActivity;
import com.example.edward.echealtyproject.R;
import com.example.edward.echealtyproject.SurveyQuestionActivity;
import com.example.edward.echealtyproject.plist.PlistHandler;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by edward on 12/17/2015.
 */
public class SingleCheckBoxView extends QuestionListviewItemView {

    PostAnswerActivity postAnswerActivity;
    HashMap<String, Object> question_hashmap;
    HashMap<String, Object> answer_hashmap;
    int TYPE;
    int listview_position;

    String answer_string;

    View listview_item;
    TextView item_name;
    ImageView checkbox_btn;
    boolean selected_status_boolean = false;

    public SingleCheckBoxView(PostAnswerActivity postAnswerActivity, HashMap<String, Object> question_hashmap, HashMap<String, Object> answer_hashmap, int TYPE, int listview_position) {
        this.postAnswerActivity = postAnswerActivity;
        this.question_hashmap = question_hashmap;
        this.answer_hashmap = answer_hashmap;
        this.TYPE = TYPE;
        this.listview_position = listview_position;
    }

    public SingleCheckBoxView(PostAnswerActivity postAnswerActivity, int TYPE, int listview_position) {
        this.postAnswerActivity = postAnswerActivity;
        this.question_hashmap = postAnswerActivity.get_question(listview_position);
        this.answer_hashmap = postAnswerActivity.get_answer(listview_position);
        this.TYPE = TYPE;
        this.listview_position = listview_position;
    }

    @Override
    public View create_question_listview_view() {
        listview_item = View.inflate(postAnswerActivity, R.layout.survey_checklist_item, null);
        final TextView single_checkbox_level3_btn = (TextView) listview_item.findViewById(R.id.level3_btn);
        item_name = (TextView) listview_item.findViewById(R.id.choice);
        item_name.setText((String) question_hashmap.get("Content_ZH-CN"));
        checkbox_btn = (ImageView) listview_item.findViewById(R.id.check_image);

        if (TYPE == Global.SURVERY_MODE) {
            if (answer_hashmap.get("Answer") != null && answer_hashmap.get("Answer").equals("1")) {
                checkbox_btn.setImageResource(R.mipmap.check_box);
            } else {
                checkbox_btn.setImageResource(R.mipmap.check_box_empty);
            }
        } else {
            checkbox_btn.setImageResource(R.mipmap.check_box_empty);
        }

        if (((String) question_hashmap.get("IsLevel3Present")).equals("1")) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final ArrayList<HashMap<String, ?>> level3_list = PlistHandler.readURLArray(API_model.getInstance().survey_item_id_questions(Global.getInstance().getMember_id(), (String) question_hashmap.get("SurveyItemID")));
                    System.out.println("level3_list:" + API_model.getInstance().survey_item_id_questions(Global.getInstance().getMember_id(), (String) question_hashmap.get("SurveyItemID")));
                    System.out.println("level3_list:" + level3_list);
                    if (level3_list.size() == 1) {
                        postAnswerActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                single_checkbox_level3_btn.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        single_checkbox_level3_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(postAnswerActivity, SurveyQuestionActivity.class);
                                intent.putExtra("level3", level3_list);
                                intent.putExtra("survey_item_id", (String) question_hashmap.get("SurveyItemID"));
                                postAnswerActivity.startActivity(intent);
                            }
                        });
                    }
                }
            }).start();
        } else {
            single_checkbox_level3_btn.setVisibility(View.GONE);
        }

        listview_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TYPE == Global.SURVERY_MODE) {

                    if (answer_hashmap.get("Answer") != null && answer_hashmap.get("Answer").equals("1")) {
                        answer_string = "";
                    } else {
                        answer_string = "1";
                    }
                    answer_hashmap.put("Answer", answer_string);
                    postAnswerActivity.post_answer(answer_string, (String) question_hashmap.get("SurveyItemID"), SingleCheckBoxView.this);
                } else {
                    checkbox_btn.setImageResource(selected_status_boolean ? R.mipmap.check_box_empty : R.mipmap.check_box);
                    if (selected_status_boolean) {
                        answer_string = "";
                    } else {
                        answer_string = "1";
                    }
                    postAnswerActivity.store_temp_record_answer(listview_position, answer_string);
                    selected_status_boolean = !selected_status_boolean;
                }
            }
        });

        return listview_item;
    }

    @Override
    public Void onPostSuccess() {
        postAnswerActivity.reload();
        return null;
    }

    @Override
    public Void onPostFail() {
        return null;
    }

    @Override
    public String call_to_post() {
        return null;
    }

}
