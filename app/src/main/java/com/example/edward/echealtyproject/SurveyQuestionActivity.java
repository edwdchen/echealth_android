package com.example.edward.echealtyproject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.edward.echealtyproject.plist.PlistHandler;
import com.example.edward.echealtyproject.question_view.ChecklistView;
import com.example.edward.echealtyproject.question_view.DatePickerView;
import com.example.edward.echealtyproject.question_view.DropdownMenuView;
import com.example.edward.echealtyproject.question_view.PickerView;
import com.example.edward.echealtyproject.question_view.SingleCheckBoxView;
import com.example.edward.echealtyproject.question_view.ToggleView;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PortUnreachableException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created by edward on 11/19/2015.
 * const HEADER = "0";
 * const PICKER = "1";
 * const SINGLE_CHECKBOX = "2";
 * const TOGGLE = "3";
 * const TEXT = "4";
 * const SLIDER = "5";
 * const DROP_DOWN_MENU = "6";
 * const DATE_PICKER = "7";
 * const CHECK_LIST = "9";
 */

/**
 * 2 main APIs
 * survey_questions
 * survey_answers
 * only toggle, checklist, single checkbox, picker have level3
 */


public class SurveyQuestionActivity extends PostAnswerActivity {

    String Attempt = null;
    ArrayList<HashMap<String, Object>> summary_activity_list;
    private Object[] temp_answer_array;//used in record mode to store selected answers
    //date picker:String
    //dropdown menu: Arraylist<String>      String: dropdownChoiceID
    private boolean first_time_init = true;
    private String SELECTED_PICKER_NUMBER, PICKER_LABEL;
    private Button finish;
    Dialog dialog;
    RequestQueue mQueue;
    String SurveyID, RecordID;
    ListView listview;
    BaseAdapter listview_adapter;
    ArrayList<HashMap<String, Object>> survey_question_list;
    ArrayList<HashMap<String, Object>> survey_answer_list;
    String TAG = "SurveyQuestionActivity";
    ArrayList<HashMap<String, String>> choice_list;//dropdown menu list shown in dialog
    ProgressDialog progressDialog;
    ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
    Runnable load_data_command;
    String survey_item_id = null;
    boolean level3_mode = false;
    Map<String, String> post_map = new HashMap<>();
    boolean survey_mode = true;
    String level2_record_item_id;
    String checklist_choice;

    boolean RECORD_EDIT_MODE = false;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_list_layout);

        progressDialog = new ProgressDialog(SurveyQuestionActivity.this);
        survey_mode = getIntent().getBooleanExtra("survey_mode?", true);

        if (getIntent().getStringExtra("Attempt") != null) {
            Attempt = getIntent().getStringExtra("Attempt");
            RECORD_EDIT_MODE = true;
        }

        if (getIntent().getSerializableExtra("level3") != null) {
            //intent as level3
            survey_question_list = (ArrayList<HashMap<String, Object>>) getIntent().getSerializableExtra("level3");
            if (!survey_mode) {
                //create empty temp_answer_array for holding temp_answers later
                temp_answer_array = new Object[survey_question_list.size()];
                level2_record_item_id = getIntent().getStringExtra("level2_record_item_id");
                checklist_choice = getIntent().getStringExtra("checklist_choice");
            }
            survey_item_id = getIntent().getStringExtra("survey_item_id");
            level3_mode = true;
            RecordID = getIntent().getStringExtra("RecordID");
        } else {
            if (survey_mode) {
                SurveyID = getIntent().getStringExtra("SurveyID");
            } else {
                RecordID = getIntent().getStringExtra("RecordID");
            }
        }

        Button back = (Button) findViewById(R.id.back_button);
        back.setText("完成");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        finish = (Button) findViewById(R.id.finish_button);
        finish.setVisibility(View.INVISIBLE);

        TextView title = (TextView) findViewById(R.id.title_textview);
        if (getIntent().getStringExtra("title") == null) {
            //level3
            title.setText("");
        } else {
//            title.setText(getUTFString(getIntent().getStringExtra("title")));//not utf-8 condition
            title.setText(getIntent().getStringExtra("title"));
        }

        listview = (ListView) findViewById(R.id.listview);
        listview_adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return survey_question_list.size() - 1;
            }

            @Override
            public HashMap<String, ?> getItem(int position) {
                return survey_question_list.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View listview_item = null;
                TextView item_name;
                switch (Integer.parseInt((String) survey_question_list.get(position).get("QuestionType"))) {
                    case 0://header
                        listview_item = View.inflate(SurveyQuestionActivity.this, R.layout.survey_question_header_item, null);
                        item_name = (TextView) listview_item.findViewById(R.id.item_name);
                        item_name.setText((String) survey_question_list.get(position).get("Name_ZH-CN"));
                        break;

                    case 1://PICKER
                        if (survey_mode) {
                            listview_item = new PickerView(SurveyQuestionActivity.this, survey_question_list.get(position), survey_answer_list.get(position), Global.SURVERY_MODE, position).create_question_listview_view();
                        } else {
                            listview_item = new PickerView(SurveyQuestionActivity.this, survey_question_list.get(position), null, Global.RECORD_MODE, position).create_question_listview_view();
                        }
                        break;

                    case 2://single checkbox
                        if (survey_mode) {
                            listview_item = new SingleCheckBoxView(SurveyQuestionActivity.this, survey_question_list.get(position), survey_answer_list.get(position), Global.SURVERY_MODE, position).create_question_listview_view();
                        } else {
                            listview_item = new SingleCheckBoxView(SurveyQuestionActivity.this, survey_question_list.get(position), null, Global.RECORD_MODE, position).create_question_listview_view();
                        }
                        break;

                    case 3://toggle

                        if (survey_mode) {
                            listview_item = new ToggleView(SurveyQuestionActivity.this, survey_question_list.get(position), survey_answer_list.get(position), Global.SURVERY_MODE, position).create_question_listview_view();
                        } else {
                            listview_item = new ToggleView(SurveyQuestionActivity.this, survey_question_list.get(position), null, Global.RECORD_MODE, position).create_question_listview_view();
                        }
                        break;

                    case 4://text
                        listview_item = View.inflate(SurveyQuestionActivity.this, R.layout.survey_question_text_item, null);
                        item_name = (TextView) listview_item.findViewById(R.id.item_name);
                        item_name.setText((String) survey_question_list.get(position).get("Content_ZH-CN"));
                        LinearLayout container1, container2;
                        container1 = (LinearLayout) listview_item.findViewById(R.id.container1);
                        container2 = (LinearLayout) listview_item.findViewById(R.id.container2);
                        TextView label1 = (TextView) listview_item.findViewById(R.id.text_label1);
                        TextView label2 = (TextView) listview_item.findViewById(R.id.text_label2);
                        label1.setText((String) survey_question_list.get(position).get("Label_ZH-CN"));
                        label2.setText((String) survey_question_list.get(position).get("Label_ZH-CN"));

                        final EditText editText1, editText2;
                        editText1 = (EditText) listview_item.findViewById(R.id.edittext1);
                        editText2 = (EditText) listview_item.findViewById(R.id.edittext2);
                        if (survey_mode) {
                            ArrayList<String> text_array = (ArrayList<String>) survey_answer_list.get(position).get("Answer");
                            //type 2 has 2 edittext
                            if ((survey_question_list.get(position).get("Type")).equals("2")) {
                                editText1.setHint("中文");
                                editText2.setHint("英文或拼音");
                                if (text_array.size() > 0) {
                                    if (null != text_array.get(0)) {
                                        editText1.setText(text_array.get(0));
                                    }
                                    if (text_array.size() > 1) {
                                        editText2.setText(text_array.get(1));
                                    }
                                }
                            } else {
                                container2.setVisibility(View.GONE);
                                if (text_array.size() > 0) {
                                    if (null != text_array.get(0)) {
                                        editText1.setText(text_array.get(0));
                                    }
                                }
                            }
                        } else {
                            //record
                            if ((survey_question_list.get(position).get("Type")).equals("2")) {
                                editText1.setHint("中文");
                                editText2.setHint("英文或拼音");
                            } else {
                                editText2.setVisibility(View.GONE);
                            }
                        }

//                        editText1.setFocusableInTouchMode(true);
//                        editText1.requestFocus();

                        editText1.setOnKeyListener(new View.OnKeyListener() {
                            public boolean onKey(View v, int keyCode, KeyEvent event) {
                                // If the event is a key-down event on the "enter" button
                                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                                    // Perform action on key press
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                    if (survey_mode) {
                                        post_edittext_content(editText1, editText2, position);
                                    } else {
                                        store_temp_record_answer(position, editText1.getText().toString() + "|" + editText2.getText().toString() + "|");

                                    }
                                    return true;
                                }
                                return false;
                            }
                        });


                        editText1.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                            public void onFocusChange(View v, boolean hasFocus) {
                                if (!hasFocus) {
                                    // code to execute when EditText loses focus
                                    if (survey_mode) {
                                        post_edittext_content(editText1, editText2, position);
                                    } else {
                                        store_temp_record_answer(position, editText1.getText().toString() + "|" + editText2.getText().toString() + "|");

                                    }

                                }
                            }
                        });

//                        editText2.setFocusableInTouchMode(true);
//                        editText2.requestFocus();

                        editText2.setOnKeyListener(new View.OnKeyListener() {
                            public boolean onKey(View v, int keyCode, KeyEvent event) {
                                // If the event is a key-down event on the "enter" button
                                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                                    // Perform action on key press
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                                    if (survey_mode) {
                                        post_edittext_content(editText1, editText2, position);
                                    } else {
                                        store_temp_record_answer(position, editText1.getText().toString() + "|" + editText2.getText().toString() + "|");

                                    }

                                    return true;
                                }
                                return false;
                            }
                        });

                        editText2.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                            public void onFocusChange(View v, boolean hasFocus) {
                                if (!hasFocus) {
                                    // code to execute when EditText loses focus
                                    if (survey_mode) {
                                        post_edittext_content(editText1, editText2, position);
                                    } else {
                                        store_temp_record_answer(position, editText1.getText().toString() + "|" + editText2.getText().toString() + "|");
                                    }
                                }
                            }
                        });
                        break;

                    case 5://SliderQuestion

                        listview_item = View.inflate(SurveyQuestionActivity.this, R.layout.survey_question_sliding_item, null);
                        item_name = (TextView) listview_item.findViewById(R.id.item_name);
                        item_name.setText((String) survey_question_list.get(position).get("Content_ZH-CN"));
                        TextView slider_description = (TextView) listview_item.findViewById(R.id.description);

                        if (null != survey_question_list.get(position).get("Description_ZH-CN") || ((String) survey_question_list.get(position).get("Description_ZH-CN")).length() > 0) {

                            slider_description.setText((String) survey_question_list.get(position).get("Description_ZH-CN"));
                        }
                        final TextView slider_level3_btn = (TextView) listview_item.findViewById(R.id.level3_btn);
                        if (survey_question_list.get(position).get("IsLevel3Present").equals("1")) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    final ArrayList<HashMap<String, ?>> level3_list = PlistHandler.readURLArray(API_model.getInstance().survey_item_id_questions(Global.getInstance().getMember_id(), (String) survey_question_list.get(position).get("SurveyItemID")));
                                    if (level3_list.size() == 1) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                slider_level3_btn.setVisibility(View.GONE);
                                            }
                                        });
                                    } else {
                                        slider_level3_btn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intent = new Intent(SurveyQuestionActivity.this, SurveyQuestionActivity.class);
                                                intent.putExtra("level3", level3_list);
                                                intent.putExtra("survey_item_id", (String) survey_question_list.get(position).get("SurveyItemID"));
                                                startActivity(intent);
                                            }
                                        });
                                    }
                                }
                            }).start();
                        } else {
                            slider_level3_btn.setVisibility(View.GONE);
                        }
                        final ArrayList<HashMap<String, String>> sliding_choice_array = (ArrayList<HashMap<String, String>>) survey_question_list.get(position).get("ChoiceArray");
                        ArrayList<String> sliding_choice_string_array = new ArrayList<>();
                        System.out.println("sliding_choice_array:" + sliding_choice_array);

                        if (sliding_choice_array != null && sliding_choice_array.size() > 0) {
                            sliding_choice_string_array.add("--");
                            for (int i = 0, length = sliding_choice_array.size(); i < length; i++) {
                                sliding_choice_string_array.add(sliding_choice_array.get(i).get("Content_ZH-CN"));
                            }
                        }

                        final TextView selected_item_name = (TextView) listview_item.findViewById(R.id.selected_choice);
                        final SeekBar comboSeekBar = (SeekBar) listview_item.findViewById(R.id.seekbar);
                        System.out.println("hehe:" + survey_answer_list);
                        if (survey_answer_list.get(position).get("Answer") == null || survey_answer_list.get(position).get("Answer").equals("")) {
                            //defalt position = 0
                            selected_item_name.setText("--");
                            comboSeekBar.setProgress(0);
                        } else {
                            //find whether there is selected choice
                            for (int i = 0, length = sliding_choice_array.size(); i < length; i++) {
                                if (survey_answer_list.get(position).get("Answer").equals(sliding_choice_array.get(i).get("SliderChoiceID"))) {
                                    selected_item_name.setText(sliding_choice_array.get(i).get("Content_ZH-CN"));
                                    comboSeekBar.setProgress(i + 1);
                                }
                            }
                        }

                        comboSeekBar.setMax(sliding_choice_array.size());
                        comboSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                if (progress == 0) {
                                    selected_item_name.setText("--");
                                } else {
                                    selected_item_name.setText(sliding_choice_array.get(progress - 1).get("Content_ZH-CN"));
                                }
                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {

                            }

                            @Override
                            public void onStopTrackingTouch(final SeekBar seekBar) {

                                StringRequest stringRequest = new StringRequest(Request.Method.POST, API_model.getInstance().survey_submit_single(),
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                Log.d(TAG, "response -> " + response);
                                                if (response.replaceAll("\\s+", "").equals("1000")) {
                                                    //problem in api return, an extra space is added in front of the result
                                                    reload();
                                                    Toast.makeText(SurveyQuestionActivity.this, "Submission success", Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(SurveyQuestionActivity.this, "Submission fail", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        reload();
                                        Log.e(TAG, error.getMessage(), error);
                                        Toast.makeText(SurveyQuestionActivity.this, "Network error", Toast.LENGTH_SHORT).show();
                                    }
                                }) {

                                    protected Map<String, String> getParams() {
                                        //在这里设置需要post的参数

                                        post_map.put("member_id", Global.getInstance().getMember_id());
                                        if (seekBar.getProgress() == 0) {
                                            post_map.put("answer", "");
                                        } else {
                                            post_map.put("answer", ((ArrayList<HashMap<String, String>>) survey_question_list.get(position).get("ChoiceArray")).get(seekBar.getProgress() - 1).get("SliderChoiceID"));
                                        }
                                        post_map.put("survey_item_id", (String) survey_question_list.get(position).get("SurveyItemID"));
                                        System.out.println("map:" + post_map);
                                        return post_map;
                                    }
                                };
                                mQueue.add(stringRequest);
                            }
                        });

                        break;

                    case 6://dropdown menu
                        if (survey_mode) {
                            listview_item = new DropdownMenuView(SurveyQuestionActivity.this, survey_question_list.get(position), survey_answer_list.get(position), true, SurveyQuestionActivity.this, position, null).getInstance();
                        } else {
                            listview_item = new DropdownMenuView(SurveyQuestionActivity.this, survey_question_list.get(position), null, false, SurveyQuestionActivity.this, position, (ArrayList<String>) temp_answer_array[position]).getInstance();
                        }
                        break;

                    case 7://date picker
                        if (survey_mode) {
                            listview_item = new DatePickerView(SurveyQuestionActivity.this, survey_question_list.get(position), survey_answer_list.get(position), Global.SURVERY_MODE, position).create_question_listview_view();
                        } else {
                            listview_item = new DatePickerView(SurveyQuestionActivity.this, survey_question_list.get(position), null, Global.RECORD_MODE, position).create_question_listview_view();
                        }
                        break;

                    case 9://check list
                        if (!survey_mode) {
                            listview_item = new ChecklistView(SurveyQuestionActivity.this, survey_question_list.get(position), null, false, SurveyQuestionActivity.this, position).getInstance();
                        } else {
                            listview_item = new ChecklistView(SurveyQuestionActivity.this, survey_question_list.get(position), survey_answer_list.get(position), true, SurveyQuestionActivity.this, position).getInstance();
                        }
                        break;
                }
                return listview_item;
            }
        };

        mQueue = Volley.newRequestQueue(SurveyQuestionActivity.this);
//        Log.i(TAG, API_model.getInstance().survey_id_questions(Global.getInstance().getMember_id(), SurveyID));

        load_data_command = new Runnable() {
            Button back;
            Button finish;

            @Override
            public void run() {

                if (first_time_init) {

                    if (level3_mode) {
                        survey_question_list = (ArrayList<HashMap<String, Object>>) getIntent().getSerializableExtra("level3");
                        HashMap<String, String> attempt_hashmap = (HashMap<String, String>) PlistHandler.readURL(API_model.getInstance().record_attempt(Global.getInstance().getMember_id(), RecordID));
                        if (Attempt == null) {
                            Attempt = attempt_hashmap.get("CurrentAttempt");
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SurveyQuestionActivity.this, Attempt, Toast.LENGTH_SHORT).show();
                            }
                        });
                        System.out.println("Attempt:" + API_model.getInstance().record_attempt(Global.getInstance().getMember_id(), RecordID));
                        System.out.println("Attempt:" + attempt_hashmap);
                        System.out.println("Attempt:" + Attempt);
                        if (survey_mode) {

                            survey_answer_list = PlistHandler.readURLArray(API_model.getInstance().survey_item_answers(Global.getInstance().getMember_id(), survey_item_id));
                        } else {
                            survey_answer_list = PlistHandler.readURLArray(API_model.getInstance().record_answers_with_level2_record_id(Global.getInstance().getMember_id(), level2_record_item_id, Attempt));
                        }
                        System.out.println(API_model.getInstance().record_answers_with_level2_record_id(Global.getInstance().getMember_id(), level2_record_item_id, Attempt));
                        System.out.println(survey_answer_list);


                    } else {
                        if (survey_mode) {
                            survey_question_list = PlistHandler.readURLArray(API_model.getInstance().survey_id_questions(Global.getInstance().getMember_id(), SurveyID));
                            survey_answer_list = PlistHandler.readURLArray(API_model.getInstance().survey_answers(Global.getInstance().getMember_id(), SurveyID));
                        } else {
                            survey_question_list = PlistHandler.readURLArray(API_model.getInstance().record_id_questions(Global.getInstance().getMember_id(), RecordID));
//                            survey_answer_list = PlistHandler.readURLArray(API_model.getInstance().record_answers(Global.getInstance().getMember_id(), RecordID));
                            HashMap<String, String> attempt_hashmap = (HashMap<String, String>) PlistHandler.readURL(API_model.getInstance().record_attempt(Global.getInstance().getMember_id(), RecordID));
                            Attempt = attempt_hashmap.get("CurrentAttempt");
                            System.out.println("Attempt:" + attempt_hashmap);
                            System.out.println("Attempt:" + Attempt);
                            //make an empty survey_answer_list
                        }
                    }
                    first_time_init = false;
                } else {
                    if (level3_mode) {
                        Collections.copy(survey_question_list, (ArrayList<HashMap<String, Object>>) getIntent().getSerializableExtra("level3"));
//                        survey_question_list = (ArrayList<HashMap<String, Object>>) getIntent().getSerializableExtra("level3");
                        Collections.copy(survey_answer_list, PlistHandler.readURLArray(API_model.getInstance().survey_item_answers(Global.getInstance().getMember_id(), survey_item_id)));
//                        survey_answer_list = PlistHandler.readURLArray(API_model.getInstance().survey_item_answers(Global.getInstance().getMember_id(), survey_item_id));
                    } else {
                        if (survey_mode) {
                            Collections.copy(survey_question_list, PlistHandler.readURLArray(API_model.getInstance().survey_id_questions(Global.getInstance().getMember_id(), SurveyID)));
                            Collections.copy(survey_answer_list, PlistHandler.readURLArray(API_model.getInstance().survey_answers(Global.getInstance().getMember_id(), SurveyID)));
                        } else {
                            Collections.copy(survey_question_list, PlistHandler.readURLArray(API_model.getInstance().record_id_questions(Global.getInstance().getMember_id(), RecordID)));
//                            Collections.copy(survey_answer_list, PlistHandler.readURLArray(API_model.getInstance().record_answers(Global.getInstance().getMember_id(), RecordID)));
                            //make an empty survey_answer_list
                            HashMap<String, String> attempt_hashmap = (HashMap<String, String>) PlistHandler.readURL(API_model.getInstance().record_attempt(Global.getInstance().getMember_id(), RecordID));
                            Attempt = attempt_hashmap.get("CurrentAttempt");

                        }
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listview_adapter.notifyDataSetChanged();
                        }
                    });
                    return;
                }

                System.out.println("survey_question_list:" + survey_question_list);
                System.out.println("survey_answer_list:" + survey_answer_list);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listview.setAdapter(listview_adapter);
                        progressDialog.dismiss();
                        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
//                                Log.i(TAG, (String) survey_question_list.get(position).get("SurveyItemID"));
                                /**
                                 * get corresponding answer info from survey_answer_list.get(position)
                                 */

                                switch (Integer.parseInt((String) survey_question_list.get(position).get("QuestionType"))) {
                                    case 0://header

                                        break;

                                    case 7://date picker
                                        dialog = new Dialog(SurveyQuestionActivity.this);
                                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                        dialog.setContentView(R.layout.date_picker_dialog);
                                        dialog.show();
                                        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker);
                                        back = (Button) dialog.findViewById(R.id.back_button);
                                        back.setText("返回");
                                        back.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                dialog.dismiss();
                                            }
                                        });
                                        finish = (Button) dialog.findViewById(R.id.finish_button);
                                        finish.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //http post
                                                System.out.println(datePicker.getYear() + "|" + datePicker.getMonth() + 1 + "|" + datePicker.getDayOfMonth());
                                                final StringBuffer stringBuffer = new StringBuffer();
                                                stringBuffer.append(datePicker.getYear());
                                                stringBuffer.append("-");
                                                stringBuffer.append(datePicker.getMonth() + 1);
                                                stringBuffer.append("-");
                                                stringBuffer.append(datePicker.getDayOfMonth());
                                                Log.i("datepicker", stringBuffer.toString());

                                                if (survey_mode) {
                                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, API_model.getInstance().survey_submit_single(),
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    Log.d(TAG, "response -> " + response);
                                                                    if (response.replaceAll("\\s+", "").equals("1000")) {
                                                                        //problem in api return, an extra space is added in front of the result
                                                                        Toast.makeText(SurveyQuestionActivity.this, "Submission success", Toast.LENGTH_SHORT).show();
                                                                        load_and_show_list();
                                                                        dialog.dismiss();
                                                                    } else {
                                                                        Toast.makeText(SurveyQuestionActivity.this, "Submission fail", Toast.LENGTH_SHORT).show();
                                                                    }
                                                                }
                                                            }, new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            Log.e(TAG, error.getMessage(), error);
                                                            Toast.makeText(SurveyQuestionActivity.this, "Network error", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }) {
                                                        protected Map<String, String> getParams() {
                                                            //在这里设置需要post的参数
                                                            post_map.put("member_id", Global.getInstance().getMember_id());
                                                            post_map.put("answer", stringBuffer.toString());
                                                            post_map.put("survey_item_id", (String) survey_question_list.get(position).get("SurveyItemID"));
                                                            System.out.println("map:" + post_map);
                                                            return post_map;
                                                        }
                                                    };
                                                    mQueue.add(stringRequest);
                                                } else {
                                                    //show selected date at item content, but not submit
                                                    TextView item_content = (TextView) listview.getChildAt(position).findViewById(R.id.content);
                                                    item_content.setText(stringBuffer.toString());
//                                                    temp_answer_array[position] = stringBuffer.toString();//save it in temp_answer_array
                                                    store_temp_record_answer(position, stringBuffer.toString());
                                                    dialog.dismiss();
                                                }
                                            }
                                        });
                                        break;
                                }
                            }
                        });

                    }
                });
            }
        };
        load_and_show_list();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void post_edittext_content(final EditText editText1, EditText editText2, final int position) {
        final String answer = editText1.getText().toString() + "|" + editText2.getText().toString() + "|";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, API_model.getInstance().survey_submit_single(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response -> " + response);
                        if (response.replaceAll("\\s+", "").equals("1000")) {
                            //problem in api return, an extra space is added in front of the result
                            Toast.makeText(SurveyQuestionActivity.this, "Submission success", Toast.LENGTH_SHORT).show();
                            reload();
                        } else {
                            Toast.makeText(SurveyQuestionActivity.this, "Submission fail", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage(), error);
                reload();
                Toast.makeText(SurveyQuestionActivity.this, "Network error", Toast.LENGTH_SHORT).show();
            }

        }) {

            protected Map<String, String> getParams() {
                //在这里设置需要post的参数
                post_map.put("member_id", Global.getInstance().getMember_id());
                post_map.put("answer", answer);
                post_map.put("survey_item_id", (String) survey_question_list.get(position).get("SurveyItemID"));
                System.out.println("map:" + post_map);
                return post_map;
            }
        };
        mQueue.add(stringRequest);
    }

    private void load_and_show_list() {
//        if (progressDialog.isShowing()) {
//            progressDialog.dismiss();
//        }
//            progressDialog.show();

        singleThreadExecutor.execute(load_data_command);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        singleThreadExecutor.shutdownNow();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "SurveyQuestion Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.edward.echealtyproject/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "SurveyQuestion Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.edward.echealtyproject/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public void post_answer(final String answer_string, final String survey_item_id, final OnPostAnswerInterface onPostAnswerInterface) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, API_model.getInstance().survey_submit_single(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "response -> " + response);
                        if (response.replaceAll("\\s+", "").equals("1000")) {
                            //problem in api return, an extra space is added in front of the result
                            Toast.makeText(SurveyQuestionActivity.this, "Submission success", Toast.LENGTH_SHORT).show();
                            onPostAnswerInterface.onPostSuccess();
//                            listview_adapter.notifyDataSetChanged();
//                            load_and_show_list();
//                            dialog.dismiss();
                        } else {
                            Toast.makeText(SurveyQuestionActivity.this, "Submission fail", Toast.LENGTH_SHORT).show();
                            onPostAnswerInterface.onPostFail();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.getMessage(), error);
                Toast.makeText(SurveyQuestionActivity.this, "Network error", Toast.LENGTH_SHORT).show();
            }
        }) {
            protected Map<String, String> getParams() {
                //在这里设置需要post的参数
                post_map.put("member_id", Global.getInstance().getMember_id());
                post_map.put("answer", answer_string);
                post_map.put("survey_item_id", survey_item_id);
                System.out.println("map:" + post_map);
                return post_map;
            }
        };
        mQueue.add(stringRequest);
    }

    @Override
    public void show_or_hide_next_level_btn(boolean is_show, final String item_id, final String record_level3_title, final String checklist_choice) {
        if (is_show) {
            if (!survey_mode) {
                if (level3_mode) {
                    finish.setText("提交");
                } else {
                    finish.setText("下页");
                    finish.setTextColor(getResources().getColor(R.color.main_button_color));
                    finish.setBackgroundResource(R.mipmap.next_btn);
                }
            }
            finish.setVisibility(View.VISIBLE);
            finish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //only used in record mode
                    if (level3_mode) {
                        //post all answers in record mode
                        //this part is not used anymore, finish btn onclick moved to store_temp part
                    } else {
                        //enter level3

                        singleThreadExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                /**
                                 * level3 content is used record id to get, but not record item id.
                                 * record item id has to be sent to level3 too
                                 * because record item id has to be submitted together in lv3
                                 */
                                final ArrayList<HashMap<String, ?>> level3_list = PlistHandler.readURLArray(API_model.getInstance().record_level3_questions(Global.getInstance().getMember_id(), RecordID));
                                System.out.println("level3_list:" + level3_list);
                                if (level3_list.size() == 1) {
                                    //only the last edit info
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            finish.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                } else {
                                    Intent intent = new Intent(SurveyQuestionActivity.this, SurveyQuestionActivity.class);
                                    intent.putExtra("level3", level3_list);
                                    intent.putExtra("title", record_level3_title);
                                    intent.putExtra("level2_record_item_id", item_id);//pass selected level2 item id to next level
                                    intent.putExtra("survey_mode?", false);
                                    intent.putExtra("checklist_choice", checklist_choice);
                                    intent.putExtra("RecordID", RecordID);
                                    startActivity(intent);
                                }
                            }
                        });
                    }
                }
            });
        } else {
            finish.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void reload() {
        load_and_show_list();
    }

    @Override
    public void store_temp_record_answer(int question_list_position, Object answer_object) {
        boolean is_show_next_btn = false;
        temp_answer_array[question_list_position] = answer_object;
        for (int i = 0; i < temp_answer_array.length; i++) {
            if (temp_answer_array[i] != null) {
                is_show_next_btn = true;
            }
            System.out.println(i + "|" + temp_answer_array[i]);
        }
        if (is_show_next_btn) {
            finish.setText("提交");
            finish.setVisibility(View.VISIBLE);
            finish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (progressDialog.isShowing()){
//                        progressDialog.dismiss();
//                    }
//                    progressDialog.show();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            //post record item id in level2 first
                            try {
                                URL url = new URL(API_model.getInstance().record_submit_single());
                                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                                conn.addRequestProperty("encoding", "UTF-8");
                                conn.setDoInput(true);
                                conn.setDoOutput(true);
                                conn.setRequestMethod("POST");

                                OutputStream os = conn.getOutputStream();
                                OutputStreamWriter osw = new OutputStreamWriter(os);
                                BufferedWriter writer = new BufferedWriter(osw);

                                List<NameValuePair> params = new ArrayList<>();
                                params.add(new BasicNameValuePair("member_id", Global.getInstance().getMember_id()));
                                if (!RECORD_EDIT_MODE) {
                                    params.add(new BasicNameValuePair("record_item_id", level2_record_item_id));
                                    params.add(new BasicNameValuePair("answer", checklist_choice));
                                }
                                params.add(new BasicNameValuePair("attempt", Attempt));
                                writer.write(getQuery(params));
                                writer.flush();

                                InputStream is = conn.getInputStream();
                                InputStreamReader isr = new InputStreamReader(is);
                                BufferedReader br = new BufferedReader(isr);
                                String line;
                                StringBuilder builder = new StringBuilder();
                                while ((line = br.readLine()) != null) {
                                    builder.append(line);
                                }

                                writer.close();
                                osw.close();
                                os.close();
                                br.close();
                                isr.close();
                                is.close();
                                System.out.println("builder:" + builder.toString());

                            } catch (ProtocolException e) {
                                e.printStackTrace();
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            //post content in level3

                            for (int i = 0; i < temp_answer_array.length; i++) {
                                if (temp_answer_array[i] != null) {
                                    final int finalI = i;
                                    try {
                                        URL url = new URL(API_model.getInstance().record_submit_single());
                                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                                        conn.addRequestProperty("encoding", "UTF-8");
                                        conn.setDoInput(true);
                                        conn.setDoOutput(true);
                                        conn.setRequestMethod("POST");

                                        OutputStream os = conn.getOutputStream();
                                        OutputStreamWriter osw = new OutputStreamWriter(os);
                                        BufferedWriter writer = new BufferedWriter(osw);

                                        List<NameValuePair> params = new ArrayList<>();
                                        if (temp_answer_array[finalI] instanceof ArrayList) {
                                            params.add(new BasicNameValuePair("answer", arraylist_to_string((ArrayList<String>) temp_answer_array[finalI])));
                                        } else {
                                            params.add(new BasicNameValuePair("answer", (String) temp_answer_array[finalI]));
                                        }
                                        params.add(new BasicNameValuePair("member_id", Global.getInstance().getMember_id()));
                                        params.add(new BasicNameValuePair("record_item_id", (String) survey_question_list.get(finalI).get("RecordItemID")));
                                        params.add(new BasicNameValuePair("attempt", Attempt));
                                        writer.write(getQuery(params));
                                        writer.flush();

                                        InputStream is = conn.getInputStream();
                                        InputStreamReader isr = new InputStreamReader(is);
                                        BufferedReader br = new BufferedReader(isr);
                                        String line;
                                        StringBuilder builder = new StringBuilder();
                                        while ((line = br.readLine()) != null) {
                                            builder.append(line);
                                        }

                                        writer.close();
                                        osw.close();
                                        os.close();
                                        br.close();
                                        isr.close();
                                        is.close();
                                        System.out.println("builder:" + builder.toString());

                                    } catch (ProtocolException e) {
                                        e.printStackTrace();
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    //get summary info
                                    summary_activity_list = PlistHandler.readURLArray(API_model.getInstance().icons(Global.getInstance().getCurrent_course_id()));
                                }
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.dismiss();
                                    Intent intent = new Intent(SurveyQuestionActivity.this, SummaryActivity.class);
                                    intent.putExtra("course_id", Global.getInstance().getCurrent_course_id());
                                    intent.putExtra("course_info", summary_activity_list);
                                    intent.putExtra("title", "");
                                    intent.putExtra("MODE", Global.RECORD_MODE);
                                    startActivity(intent);
                                }
                            });
                        }
                    }).start();
                }
            });
        }
    }

    private String arraylist_to_string(ArrayList<String> o) {
        String result = "";
        boolean first_time = true;
        for (String string_object : o) {
            if (first_time) {
                result = string_object;
                first_time = false;
            } else {
                result = result + "|" + string_object;
            }
        }
        return result;
    }

    public Object[] getTemp_answer_array() {
        return temp_answer_array;
    }

    public void setTemp_answer_array(Object[] temp_answer_array) {
        this.temp_answer_array = temp_answer_array;
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (NameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");
            System.out.println("pair:" + pair);
            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }
        return result.toString();
    }
}