package com.example.edward.echealtyproject;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by edward on 11/18/2015.
 */
public class NavigationActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    boolean first_time;

    String[] left_module_string = new String[]{"个人资料", "遗传病风险", "环境致病因素", "传染病预防", "认识过敏风险",
            "肿瘤预防", "心理与社交描绘", "日常生活活动", "体能锻炼与运动"};

    String[] right_module_string = new String[]{"引言与导航", "未被诊断的问题", "慢性病与癌症", "创伤与手术史",
            "其他已确诊的疾病", "用药记录", "过往不良药物反应", "医疗机构与人员", "医案概述与详述"};

    ProgressDialog progressDialog;

    ImageView laser_flower_imageview;
    LinearLayout main_container;

    ListView left_listview,right_listview;

    BaseAdapter left_adapter,right_adapter;

    int initial_rotation_angle = 80;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_layout);
        main_container = (LinearLayout) findViewById(R.id.main_container);
        laser_flower_imageview = (ImageView) findViewById(R.id.laser_flower);
        laser_flower_imageview.setImageResource(R.mipmap.laser_flower);
        laser_flower_imageview.setVisibility(View.VISIBLE);
        progressDialog = new ProgressDialog(NavigationActivity.this);
        progressDialog.show();
        sharedPreferences = getSharedPreferences("echealth", 0);
        first_time = sharedPreferences.getBoolean("first_time", true);

        TextView welcome = (TextView) findViewById(R.id.welcome);
        welcome.setText("欢迎，" + Global.getInstance().getUser_name());

        left_listview = (ListView) findViewById(R.id.left_listview);
        right_listview = (ListView) findViewById(R.id.right_listview);

        prepareAnimation();

        TextView logout = (TextView) findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //clear info in sharedpreference
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("user_name", "");
                editor.putString("password", "");
                editor.putString("member_id", "");
                editor.commit();
                finish();
            }
        });

        left_adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return left_module_string.length;
            }

            @Override
            public String getItem(int position) {
                return left_module_string[position];
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = new TextView(NavigationActivity.this);
                textView.setTextAppearance(NavigationActivity.this, android.R.style.TextAppearance_Medium);
                textView.setText(left_module_string[position]);
                textView.setPadding(20, 20, 20, 20);
                textView.setTextColor(Color.WHITE);
                    textView.setBackgroundColor(getResources().getColor(R.color.main_button_color));

                textView.setTypeface(null, Typeface.BOLD);
                textView.setGravity(Gravity.CENTER_VERTICAL);

                int screenHeight = (NavigationActivity.this).getWindowManager()
                        .getDefaultDisplay().getHeight();

                textView.setLayoutParams(new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, screenHeight / 12));

                textView.setRotationX(initial_rotation_angle);

                return textView;

            }
        };

        left_listview.setAdapter(left_adapter);

        right_adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return right_module_string.length;
            }

            @Override
            public String getItem(int position) {
                return right_module_string[position];
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = new TextView(NavigationActivity.this);
                textView.setTextAppearance(NavigationActivity.this, android.R.style.TextAppearance_Medium);
                textView.setText(right_module_string[position]);
                textView.setPadding(20, 20, 20, 20);
                textView.setTextColor(Color.WHITE);
                if (position == 0) {//引言与导航
                    if (first_time) {
                        textView.setBackgroundColor(getResources().getColor(R.color.main_button_color));
                    } else {
                        textView.setBackgroundColor(getResources().getColor(R.color.highlight_color));
                    }
                } else {
                    textView.setBackgroundColor(getResources().getColor(R.color.main_button_color));
                }

                textView.setTypeface(null, Typeface.BOLD);
                textView.setGravity(Gravity.CENTER_VERTICAL);

                int screenHeight = (NavigationActivity.this).getWindowManager().getDefaultDisplay().getHeight();

                textView.setLayoutParams(new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, screenHeight / 12));

                textView.setRotationX(initial_rotation_angle);

                return textView;
            }
        };

        right_listview.setAdapter(right_adapter);

        left_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(NavigationActivity.this, ModuleActvity.class);
                int course_id = 0;
                switch (position) {
                    case 0://个人资料
                        course_id = 1;
                        break;
                    case 1://遗传病风险
                        course_id = 2;
                        break;
                    case 2://环境致病因素
                        course_id = 3;
                        break;
                    case 3://传染病预防
                        course_id = 4;
                        break;
                    case 4://认识过敏风险
                        course_id = 5;
                        break;
                    case 5://肿瘤预防
                        course_id = 6;
                        break;
                    case 6://心理
                        course_id = 7;
                        break;
                    case 7://日常生活活动
                        course_id = 8;
                        break;
                    case 8:// 体能锻炼与运动
                        course_id = 9;
                        break;
                }
                Global.getInstance().setCurrent_course_id(String.valueOf(course_id));
                intent.putExtra("course_name", ((TextView) view).getText().toString());
                intent.putExtra("course_id", course_id);
                startActivity(intent);
            }
        });

        right_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(NavigationActivity.this, ModuleActvity.class);
                int course_id = 0;
                switch (position) {
                    case 0://引言与导航
                        course_id = -1;
                        if (first_time) {
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean("first_time", false);
                            editor.commit();
                        }
                        break;
                    case 1:
                        course_id = 10;
                        break;
                    case 2:
                        course_id = 11;
                        break;
                    case 3:
                        course_id = 12;
                        break;
                    case 4:
                        course_id = 13;
                        break;
                    case 5:
                        course_id = 14;
                        break;
                    case 6:
                        course_id = 15;
                        break;
                    case 7:
                        course_id = 16;
                        break;
                    case 8:
                        course_id = 0;
                        break;
                }
                Global.getInstance().setCurrent_course_id(String.valueOf(course_id));
                intent.putExtra("course_name", ((TextView) view).getText().toString());
                intent.putExtra("course_id", course_id);
                startActivity(intent);
            }
        });

        progressDialog.dismiss();
    }

    private void prepareAnimation() {
        new CountDownTimer(2500, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                AnimatorSet translationSet = new AnimatorSet();
                translationSet.playTogether(
                        ObjectAnimator.ofFloat(left_listview,"translationY",-2000,left_listview.getY()),
                        ObjectAnimator.ofFloat(right_listview,"translationY",2000,right_listview.getY())
                );

                main_container.setVisibility(View.VISIBLE);
                laser_flower_imageview.setVisibility(View.GONE);
                AnimatorListenerAdapter listenerAdapter = new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        for (int i = 0; i<9;i++){
                            ObjectAnimator.ofFloat(left_listview.getChildAt(i),"RotationX",initial_rotation_angle,0).setDuration(1000).start();
                            ObjectAnimator.ofFloat(right_listview.getChildAt(i),"RotationX",initial_rotation_angle,0).setDuration(1000).start();
                        }
                    }
                };
                translationSet.addListener(listenerAdapter);
                translationSet.setDuration(1000).start();
            }
        }.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences = getSharedPreferences("echealth", 0);
        String name = sharedPreferences.getString("user_name", "");
        String password = sharedPreferences.getString("password", "");
        String member_id = sharedPreferences.getString("member_id", "");

        if (name.length() == 0 || password.length() == 0) {

        } else {
            Global.getInstance().setUser_name(name);
            Global.getInstance().setPassword(password);
            Global.getInstance().setMember_id(member_id);
        }
    }
}
