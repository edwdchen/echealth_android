package com.example.edward.echealtyproject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by edward on 11/17/2015.
 */
public class RegisterActivity extends AppCompatActivity {
    EditText name_input, password_input, password_confirm;
    Custom_toggle_button custom_toggle_button;
    Button back_btn, finish_btn;
    RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.register_layout);

        mQueue = Volley.newRequestQueue(RegisterActivity.this);

        TextView activity_title = (TextView) findViewById(R.id.title_textview);
        activity_title.setText("账户注册");
        custom_toggle_button = (Custom_toggle_button) findViewById(R.id.gender_switch);

        back_btn = (Button) findViewById(R.id.back_button);
        finish_btn = (Button) findViewById(R.id.finish_button);

        name_input = (EditText) findViewById(R.id.name_input);
        password_input = (EditText) findViewById(R.id.password_input);
        password_confirm = (EditText) findViewById(R.id.password_input_confirm);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        finish_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (custom_toggle_button.getSelect_number() == 0 || name_input.getText().toString().length() == 0 || password_input.getText().toString().length() == 0 || password_confirm.getText().length() == 0) {
                    Toast.makeText(RegisterActivity.this, "你在填写了所有资料后才可注册。", Toast.LENGTH_SHORT).show();
                } else {
                    String email = name_input.getText().toString();
                    Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
                    Matcher m = p.matcher(email);
                    boolean matchFound = m.matches();
                    if (matchFound) {
                        //your work here
                        if (password_input.getText().toString().equals(password_confirm.getText().toString())) {
                            //submit
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, API_model.getInstance().survey_submit_single(),
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {

                                            if (response.replaceAll("\\s+", "").equals("1000")) {
                                                //problem in api return, an extra space is added in front of the result
                                                Toast.makeText(RegisterActivity.this, "Submission success", Toast.LENGTH_SHORT).show();

                                            } else {
                                                Toast.makeText(RegisterActivity.this, "你在注册过程中有错误，请稍后再试。", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(RegisterActivity.this, "Network error", Toast.LENGTH_SHORT).show();
                                }
                            }) {

                                protected Map<String, String> getParams() {
                                    //在这里设置需要post的参数
                                    /**
                                     * POST parameters:
                                     - username
                                     - password
                                     - gender: 1:Male; 0:Female
                                     - platformid: 8:iPhone 11:android phone
                                     */

                                    HashMap<String, String> post_map = new HashMap<String, String>();
                                    if (custom_toggle_button.getSelect_number() == 1) {
                                        post_map.put("gender", "1");
                                    } else if (custom_toggle_button.getSelect_number() == 2) {
                                        post_map.put("gender", "0");
                                    }
                                    post_map.put("username", name_input.getText().toString());
                                    post_map.put("password", password_input.getText().toString());
                                    post_map.put("platformid", "11");

                                    System.out.println("map:" + post_map);
                                    return post_map;
                                }
                            };
                        } else {
                            Toast.makeText(RegisterActivity.this, "请检查清楚及确保两个密码是相同的。", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(RegisterActivity.this, "你输入的电邮地址无效。请选用其他电邮地址。", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
