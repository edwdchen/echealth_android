package com.example.edward.echealtyproject;

/**
 * Created by edward on 1/22/2016.
 */

import android.os.Bundle;
import android.widget.ListView;

/**
 * Created by edward on 11/19/2015.
 * const HEADER = "0";
 * const PICKER = "1";
 * const SINGLE_CHECKBOX = "2";
 * const TOGGLE = "3";
 * const TEXT = "4";
 * const SLIDER = "5";
 * const DROP_DOWN_MENU = "6";
 * const DATE_PICKER = "7";
 * const CHECK_LIST = "9";
 */

public class QuestionActivity extends PostAnswerActivity{

    /**
     * this activity is used for survey and record ,including level2 and level3
     */
    ListView listview;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_list_layout);
        listview = (ListView) findViewById(R.id.listview);


    }
}
