package com.example.edward.echealtyproject;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by edward on 11/23/2015.
 */
public class SingleChoiceListview extends ListView{

    public int getSelect_item_number() {
        return select_item_number;
    }

    public void setSelect_item_number(int select_item_number) {
        this.select_item_number = select_item_number;
    }

    private int select_item_number = -1;
    SingleChoiceAdapter singleChoiceAdapter;
    public SingleChoiceListview(Context context) {
        super(context);
    }

    public SingleChoiceListview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setSingleChoiceAdapter(SingleChoiceAdapter singleChoiceAdapter){
        setAdapter(singleChoiceAdapter);
        this.singleChoiceAdapter = singleChoiceAdapter;
    }
}
