package com.example.edward.echealtyproject;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by edward on 11/18/2015.
 */
public class DisclaimerActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disclaimer);
        TextView title = (TextView) findViewById(R.id.title_textview);
        title.setText("法律声明");
        Button button = (Button) findViewById(R.id.finish_button);
        button.setVisibility(View.INVISIBLE);
        Button back = (Button) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        WebView webview = (WebView) findViewById(R.id.webview);
        webview.loadUrl(API_model.getInstance().disclaimer());
    }
}
