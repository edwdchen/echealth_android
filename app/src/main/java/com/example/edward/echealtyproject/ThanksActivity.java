package com.example.edward.echealtyproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by edward on 11/17/2015.
 */
public class ThanksActivity extends Activity{
    /**
     * type
     * 0:thanks
     * 1:introduction
     * 2:navigation
     */
    int type =0;
    TextView title;
    TextView content;
    Button finish;
    Button back;
    Button video_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thanks_layout);
        type = getIntent().getIntExtra("type",0);
        title = (TextView) findViewById(R.id.title_textview);
        title = (TextView) findViewById(R.id.title_textview);
        finish = (Button) findViewById(R.id.finish_button);
        video_btn = (Button) findViewById(R.id.play_video);
        content = (TextView) findViewById(R.id.content);
        content.setMovementMethod(new ScrollingMovementMethod());

        finish.setVisibility(View.INVISIBLE);
        back = (Button) findViewById(R.id.back_button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        switch (type){
            case 0:
                title.setText("答谢");
                video_btn.setVisibility(View.GONE);
                content.setText("Pioneer Communications Ltd, the producer of EC Health, would like to express sincere thanks to our staff, content writers, graphics designers, translators, legal advisers and idea contributors in the development of this app. Credit must also go to the programmers at IT Wake Ltd. for their effort in the endless corrections and updates necessary in the development of an app of this nature.");
                break;
            case 1:
                title.setText("引言");
                content.setText("世卫组织提出了「全民健康」的概念，目标是广泛大众均可获得基本的医疗与保健服务，而且也重新定义了「健康」的标准：健康不单是远离疾病，而且更意味着生活素质的提升，从而容许个人积极为国家和家庭作出贡献。要实现「全民健康」，除需要提供各类救死扶伤的医院和药物之外，更需要一个完善的公共保健系统去降低营养不良、卫生欠佳及健康知识缺乏等患病风险。这个系统的运作有赖于全面的社会发展，包括不断优化在农业、工业、教育和资讯等领域的方案与协作。(EC健)正是根据世卫的理念而设计的资讯交流平台。\n" +
                        "\n" +
                        "医疗纪录是您和医护团队沟通的第一桥梁。透过对繁琐复杂的症状进行智能化的排列分序，医生可作出更准确的诊断。全面的疾病风险评估，除可辅助诊断之外，也有助于制定预防疾病的措施。慢性疾病患者，需要长期与不同领域的医疗人员互相交流，而一个连贯性的纪录则是实现自我管理的必要工具。透过使用(EC健)的互动式问卷功能，您可随时以准确、详细和连贯的方式，储存关于症状、风险与疾病方面的资料，让您尽可能避免重复的医疗程序，也可以节省整体社区资源。");
                video_btn.setVisibility(View.GONE);
                break;
            
            case 2:
                content.setText("EC操作非常简单。首页详细列出各模组的标題，而內容歸納於三層式問卷，循序渐进。用户可独立使用各模组，请先填妥个人资料，再按实际需要填写其他模组。模组按照「风险导致疾病，疾病引发症状，确诊后的治疗」这个医学概念而定，並分为风险、疾病、症状和治疗四个范畴。您可以随时到各模组的摘要检视已填妥的资料；更可于求诊前到医案概述或医案详述，一览所有模组合并的总结，然后列印出来让医护人员参考。\n\n这个程式有两种资料处理程序。首页左列从「个人资料」到「体能锻炼与运动」为风险模组，已填好的资料会同时保留于问卷和摘要中。首页右列如「慢性病与癌症」、「创伤与手术史」和「其他已确诊的疾病」等为疾病管理模组，已填好的资料会送往摘要的部分，而问卷部分则会清空以供继续填写。\n\n请尽可能细心完成所有问卷。这不仅有助您建立健康纪录，也能使您更了解自己的健康状况。希望用得开心，祝您身心健康！");
                title.setText("导航");
                video_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ThanksActivity.this,Video_land_activity.class);
                        startActivity(intent);
                    }
                });
                break;
        }

    }
}
