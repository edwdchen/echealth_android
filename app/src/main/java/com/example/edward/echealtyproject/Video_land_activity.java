package com.example.edward.echealtyproject;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by edward on 11/24/2015.
 */
public class Video_land_activity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VideoView videoView = new VideoView(Video_land_activity.this);
        videoView.setMediaController(new MediaController(this));
        String path = "android.resource://" + getPackageName() + "/" + R.raw.animation;
        System.out.println(path);
        videoView.setVideoURI(Uri.parse(path));
        setContentView(videoView);
        videoView.start();
    }
}
