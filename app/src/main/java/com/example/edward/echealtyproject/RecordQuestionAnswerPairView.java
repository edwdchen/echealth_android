package com.example.edward.echealtyproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

/**
 * Created by edward on 12/21/2015.
 */
public class RecordQuestionAnswerPairView extends LinearLayout{

    Context context;
    HashMap<String,Object> item_info;

    public RecordQuestionAnswerPairView(Context context,HashMap<String, Object> item_info) {
        super(context);
        this.context = context;
        this.item_info = item_info;
        initView();
    }

    public RecordQuestionAnswerPairView(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    private void initView() {
        System.out.println("item_info:"+item_info);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.record_question_answer_pair_view_layout, this);

        TextView question_textview = (TextView) findViewById(R.id.question);
        TextView answer_textview = (TextView) findViewById(R.id.answer);

        if (null != item_info){
            question_textview.setText(item_info.get("QuestionContent_ZHCN").toString());
            answer_textview.setText(item_info.get("AnswerContent_ZHCN").toString().replace("|","\n").replace("00:00:00",""));
        }
    }
}
