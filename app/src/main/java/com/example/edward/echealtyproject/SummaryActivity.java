package com.example.edward.echealtyproject;


import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.edward.echealtyproject.plist.PlistHandler;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by edward on 12/17/2015.
 * 4 params
 * title
 * mode
 * course_id
 * course_info
 */
public class SummaryActivity extends AppCompatActivity {

    private static final int EDIT_ACITVITY = 123456;
    String course_id;
    int MODE;

    Thread load_data_thread;

    TextView record_indicator;

    BaseAdapter listview_adapter;
    ListView summary_listview;
    ArrayList<HashMap<String, Object>> summary_arraylist;
    RequestQueue mQueue;

    ArrayList<RecordBundle> record_arraylist = new ArrayList<>();
    private boolean first_time_init = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary_layout);

        mQueue = Volley.newRequestQueue(SummaryActivity.this);
        String title_string = getIntent().getStringExtra("title");

        MODE = getIntent().getIntExtra("MODE", Global.SURVERY_MODE);

        course_id = getIntent().getStringExtra("course_id");

        if (MODE == Global.RECORD_MODE) {
            summary_arraylist = (ArrayList<HashMap<String, Object>>) getIntent().getSerializableExtra("course_info");
        }

        record_indicator = (TextView) findViewById(R.id.record_indicator);

        Button back_btn, finish_btn;
        back_btn = (Button) findViewById(R.id.back_button);
        finish_btn = (Button) findViewById(R.id.finish_button);
        finish_btn.setVisibility(View.INVISIBLE);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView title_textview = (TextView) findViewById(R.id.title_textview);

        title_textview.setText(title_string);

        summary_listview = (ListView) findViewById(R.id.listview);

        listview_adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                if (MODE == Global.SURVERY_MODE) {
                    return summary_arraylist.size();
                } else {
                    return record_arraylist.size();
                }
            }

            @Override
            public Object getItem(int position) {
                return summary_arraylist.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {

                View listview_item = null;
                if (MODE == Global.SURVERY_MODE) {
                    listview_item = View.inflate(SummaryActivity.this, R.layout.survey_summary_item, null);

                    final HashMap<String, Object> item_info_hashmap = summary_arraylist.get(position);
                    final TextView item_name, item_content;
                    EditDeleteView editDeleteView = (EditDeleteView) listview_item.findViewById(R.id.edit_delete_btn);
                    item_name = (TextView) listview_item.findViewById(R.id.item_name);
                    item_content = (TextView) listview_item.findViewById(R.id.item_content);
                    item_name.setText((String) item_info_hashmap.get("Name_ZH-CN"));
                    item_content.setText((String) item_info_hashmap.get("Content_ZH-CN"));

                    editDeleteView.setOnEditDeleteListener(new OnEditDeleteListener() {
                        @Override
                        public void onEdit() {
                            Intent intent = new Intent();
                            intent.putExtra("survey_mode?", true);
                            intent.setClass(SummaryActivity.this, SurveyQuestionActivity.class);
                            intent.putExtra("SurveyID", (String) summary_arraylist.get(position).get("SurveyID"));
                            intent.putExtra("title", (String) item_info_hashmap.get("Name_ZH-CN"));
                            startActivity(intent);
                        }

                        @Override
                        public void onDelete() {
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, API_model.getInstance().survey_delete_answer(),
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {

                                            if (response.replaceAll("\\s+", "").equals("1000")) {
                                                //problem in api return, an extra space is added in front of the result
                                                Toast.makeText(SummaryActivity.this, "Submission success", Toast.LENGTH_SHORT).show();
                                                summary_arraylist.get(position).put("Content", "");
                                                listview_adapter.notifyDataSetChanged();
                                            } else {
                                                Toast.makeText(SummaryActivity.this, "Submission fail", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(SummaryActivity.this, "Network error", Toast.LENGTH_SHORT).show();
                                }
                            }) {
                                protected Map<String, String> getParams() {
                                    //在这里设置需要post的参数
                                    HashMap<String, String> post_map = new HashMap<String, String>();
                                    post_map.put("member_id", Global.getInstance().getMember_id());
                                    post_map.put("survey_id", (String) summary_arraylist.get(position).get("SurveyID"));
                                    System.out.println("map:" + post_map);
                                    return post_map;
                                }
                            };

                            mQueue.add(stringRequest);
                        }
                    });
                } else {
                    listview_item = View.inflate(SummaryActivity.this, R.layout.record_summary_item, null);

                    RecordBundle recordBundle = record_arraylist.get(position);

                    HashMap<String, Object> record_id_info = recordBundle.getRecord_id_info();

                    ArrayList<ArrayList<HashMap<String, Object>>> record_answer_info = recordBundle.getRecord_answer_info();

                    TextView listview_header = (TextView) listview_item.findViewById(R.id.listview_header);
                    listview_header.setText((String) record_id_info.get("Name_ZH-CN"));

                    ExpendedList expendedList = (ExpendedList) listview_item.findViewById(R.id.expanded_listview);

                    RecordSummaryItemAdapter recordSummaryItemAdapter = new RecordSummaryItemAdapter(SummaryActivity.this, (String) record_id_info.get("RecordID"), record_answer_info);

                    expendedList.setAdapter(recordSummaryItemAdapter);
                }
                return listview_item;
            }
        };

        summary_listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });

    }


    class RecordBundle {
        HashMap<String, Object> record_id_info;

        ArrayList<ArrayList<HashMap<String, Object>>> record_answer_info;

        public HashMap<String, Object> getRecord_id_info() {
            return record_id_info;
        }

        public void setRecord_id_info(HashMap<String, Object> record_id_info) {
            this.record_id_info = record_id_info;
        }

        public ArrayList<ArrayList<HashMap<String, Object>>> getRecord_answer_info() {
            return record_answer_info;
        }

        public void setRecord_answer_info(ArrayList<ArrayList<HashMap<String, Object>>> record_answer_info) {
            this.record_answer_info = record_answer_info;
        }

        public RecordBundle(HashMap<String, Object> record_id_info, ArrayList<ArrayList<HashMap<String, Object>>> record_answer_info) {
            this.record_id_info = record_id_info;
            this.record_answer_info = record_answer_info;
        }
    }

    void clone_textview(TextView src_textview, TextView des_textview) {
        des_textview.setText(src_textview.getText());
        des_textview.setTextColor(src_textview.getTextColors());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_ACITVITY) {
            if (resultCode == RESULT_OK) {
                // The user picked a contact.
                // The Intent's data Uri identifies which contact was selected.
                // Do something with the contact here (bigger example below)
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        load_data_thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (MODE == Global.SURVERY_MODE) {
                    if (!first_time_init){
                        Collections.copy(summary_arraylist,PlistHandler.readURLArray(API_model.getInstance().survey_summary(Global.getInstance().getMember_id(), course_id)));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                listview_adapter.notifyDataSetChanged();
                            }
                        });
                        return;
                    }else {
                        summary_arraylist = PlistHandler.readURLArray(API_model.getInstance().survey_summary(Global.getInstance().getMember_id(), course_id));
                        first_time_init = false;
                    }
                } else {

                    //load record_id_summary into record_arraylist
                    record_arraylist.clear();
                    for (int i = 0, length = summary_arraylist.size() - 1; i < length; i++) {
                        //example: http://echealth.iclassu.com/ios/beta/record_summary.php?record_id=2&member_id=2
                        ArrayList<ArrayList<HashMap<String, Object>>> single_record_id_info = PlistHandler.readURLArray(API_model.getInstance().record_summary(Global.getInstance().getMember_id(), (String) summary_arraylist.get(i).get("RecordID")));

                        if (null != single_record_id_info && single_record_id_info.size() > 0) {
                            //only add content not null item
                            record_arraylist.add(new RecordBundle(summary_arraylist.get(i), single_record_id_info));
                        }
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        summary_listview.setAdapter(listview_adapter);
                    }
                });
            }
        });

        load_data_thread.start();
    }

}