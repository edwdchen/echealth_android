package com.example.edward.echealtyproject;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by edward on 11/24/2015.
 */
public class ExpendedList extends ListView{
    public ExpendedList(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(
                Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
