package com.example.edward.echealtyproject.question_view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.edward.echealtyproject.ExpendedList;
import com.example.edward.echealtyproject.OnPostAnswerInterface;
import com.example.edward.echealtyproject.PostAnswerActivity;
import com.example.edward.echealtyproject.PostAnswerInterface;
import com.example.edward.echealtyproject.R;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by edward on 12/2/2015.
 */
public class ChecklistView implements OnPostAnswerInterface {

    String answer_String;
    public String getAnswer_String() {
        return answer_String;
    }

    public void setAnswer_String(String answer_String) {
        this.answer_String = answer_String;
    }
    Context mContext;
    View checklistView;
    TextView item_name;
    boolean survey_mode;
    int Type = 1;//1:single 2:multi
    String survey_item_id;
    String record_item_id;
    BaseAdapter checklist_adapter;
    int single_choice_selected_number = -1;

    boolean[] record_level3_multi_choice_selected_status_array;

    HashMap<String, ?> question_hashmap, answer_hashmap;
    //question:http://echealth.iclassu.com/ios/survey_questions.php?survey_id=24&member_id=2
    //answer sample:http://echealth.iclassu.com/ios/survey_answers.php?survey_id=24&member_id=2
    ArrayList<HashMap<String, String>> checklist_choice_array;
    ArrayList<HashMap<String, String>> answer_arraylist;
    int listview_position;
    PostAnswerInterface postAnswerInterface;

    public ChecklistView(Context context, HashMap<String, ?> question_hashmap, HashMap<String, ?> answer_hashmap, boolean survey_mode, PostAnswerInterface postAnswerInterface,int listview_position) {
        mContext = context;
        this.survey_mode = survey_mode;
        this.question_hashmap = question_hashmap;
        this.answer_hashmap = answer_hashmap;
        this.listview_position = listview_position;
        Type = Integer.parseInt((String) question_hashmap.get("Type"));
        if (survey_mode) {
            answer_arraylist = (ArrayList<HashMap<String, String>>) answer_hashmap.get("Answer");
            survey_item_id = (String) question_hashmap.get("SurveyItemID");
        } else {
            //record item id != record id
            record_item_id = (String) question_hashmap.get("RecordItemID");
            //record item id is used to get checklist info, record id is to access level 3 info
        }
//        Toast.makeText(context, record_item_id, Toast.LENGTH_SHORT).show();
        this.postAnswerInterface = postAnswerInterface;
        initView();
    }

    public ChecklistView(PostAnswerActivity postAnswerActivity, boolean survey_mode,int listview_position) {
        mContext = postAnswerActivity;
        this.survey_mode = survey_mode;
        this.question_hashmap = postAnswerActivity.get_question(listview_position);
        this.answer_hashmap = postAnswerActivity.get_answer(listview_position);
        this.listview_position = listview_position;
        Type = Integer.parseInt((String) question_hashmap.get("Type"));
        if (survey_mode) {
            answer_arraylist = (ArrayList<HashMap<String, String>>) answer_hashmap.get("Answer");
            survey_item_id = (String) question_hashmap.get("SurveyItemID");
        } else {
            //record item id != record id
            record_item_id = (String) question_hashmap.get("RecordItemID");
            //record item id is used to get checklist info, record id is to access level 3 info
        }
//        Toast.makeText(context, record_item_id, Toast.LENGTH_SHORT).show();
        this.postAnswerInterface = postAnswerActivity;
        initView();
    }


    private void initView() {
        checklistView = View.inflate(mContext, R.layout.survey_question_checklist_item, null);
        item_name = (TextView) checklistView.findViewById(R.id.item_name);
        item_name.setText((String) question_hashmap.get("Content_ZH-CN"));
        checklist_choice_array = (ArrayList<HashMap<String, String>>) question_hashmap.get("ChoiceArray");

        System.out.println("checklist_choice_array:"+checklist_choice_array);

        final ExpendedList checklist_listview = (ExpendedList) checklistView.findViewById(R.id.checklist_listview);
        //only used in level3 record mode
        record_level3_multi_choice_selected_status_array = new boolean[checklist_choice_array.size()];

        checklist_adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return checklist_choice_array.size();
            }

            @Override
            public HashMap<String, String> getItem(int position) {
                return checklist_choice_array.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (checklist_choice_array.get(position).get("ChecklistChoiceID").equals("-1")) {
                    //checklist divider header
                    TextView header_view = new TextView(mContext);
                    header_view.setText(checklist_choice_array.get(position).get("Content_ZH-CN"));
                    header_view.setBackgroundColor(Color.GRAY);
                    header_view.setGravity(Gravity.CENTER_VERTICAL);
                    header_view.setPadding(5, 5, 5, 5);
                    header_view.setTextAppearance(mContext, android.R.style.TextAppearance_Medium);
                    header_view.setTypeface(null, Typeface.BOLD);
                    header_view.setTextColor(Color.WHITE);
                    return header_view;
                } else {

                    if (Type == 1) {
                        //single
                        View checklist_item_view = View.inflate(mContext, R.layout.choice_item, null);
                        TextView textView = (TextView) checklist_item_view.findViewById(R.id.choice_text);
                        textView.setText(checklist_choice_array.get(position).get("Content_ZH-CN"));
                        ImageView imageView = (ImageView) checklist_item_view.findViewById(R.id.choice_button);
                        if (survey_mode) {
                            if (answer_arraylist.get(position).get("IsChecked").equals("1")) {
                                imageView.setImageResource(R.mipmap.check_box);
                            } else {
                                imageView.setImageResource(R.mipmap.check_box_empty);
                            }
                        } else {
                            //no need to get answer in record mode
                            if (single_choice_selected_number == position) {
                                imageView.setImageResource(R.mipmap.radio_button);
                            } else {
                                imageView.setImageResource(R.mipmap.radio_button_empty);
                            }
                        }
                        return checklist_item_view;
                    } else {
                        //multi
                        View checklist_item_view = View.inflate(mContext, R.layout.survey_checklist_item, null);
                        TextView checklist_level3_btn = (TextView) checklist_item_view.findViewById(R.id.level3_btn);
                        checklist_level3_btn.setVisibility(View.GONE);
                        TextView textView = (TextView) checklist_item_view.findViewById(R.id.choice);
                        textView.setText(checklist_choice_array.get(position).get("Content_ZH-CN"));
                        ImageView imageView = (ImageView) checklist_item_view.findViewById(R.id.check_image);
                        if (survey_mode) {
                            if (answer_arraylist.get(position).get("IsChecked").equals("1")) {
                                imageView.setImageResource(R.mipmap.check_box);
                            } else {
                                imageView.setImageResource(R.mipmap.check_box_empty);
                            }
                        } else {
                            if (record_level3_multi_choice_selected_status_array[position]) {

                                imageView.setImageResource(R.mipmap.check_box);
                            } else {
                                imageView.setImageResource(R.mipmap.check_box_empty);
                            }
                        }

                        return checklist_item_view;
                    }
                }

            }
        };
        checklist_listview.setAdapter(checklist_adapter);

        checklist_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (survey_mode) {
                    if (Type == 1) {
                        //single
                        final StringBuffer answer_buffer = new StringBuffer();
                        for (int i = 0, length = answer_arraylist.size(); i < length; i++) {

                            if (i == position) {
                                answer_arraylist.get(i).put("IsChecked", "1");
                                answer_buffer.append("1");
                            } else {
                                answer_arraylist.get(i).put("IsChecked", "0");
                                answer_buffer.append("0");
                            }
                            answer_buffer.append("|");
                            answer_String = "";
                            if (answer_buffer.length() > 1) {
                                answer_String = answer_buffer.substring(0, answer_buffer.length() - 1);
                            }
                            postAnswerInterface.post_answer(answer_String, survey_item_id, ChecklistView.this);
                        }
                    } else {
                        //multi
                        if (answer_arraylist.get(position).get("IsChecked").equals("1")) {
                            answer_arraylist.get(position).put("IsChecked", "0");
                        } else {
                            answer_arraylist.get(position).put("IsChecked", "1");
                        }
                        final StringBuffer answer_buffer = new StringBuffer();
                        for (int i = 0, length = answer_arraylist.size(); i < length; i++) {
                            if (answer_arraylist.get(i).get("IsChecked").equals("1")) {
                                answer_buffer.append(answer_arraylist.get(i).get("ChecklistChoiceID"));
                                answer_buffer.append("|");
                            }
                        }
                        answer_String = "";
                        if (answer_buffer.length() > 1) {
                            answer_String = answer_buffer.substring(0, answer_buffer.length() - 1);
                        }
                        postAnswerInterface.post_answer(answer_String, survey_item_id, ChecklistView.this);
                    }

                } else {
                    //record mode

                    if (Type == 1) {
                        //single
                        if (position == single_choice_selected_number) {
                            single_choice_selected_number = -1;
                        } else {
                            single_choice_selected_number = position;
                        }
                        ((BaseAdapter) parent.getAdapter()).notifyDataSetChanged();

                        //notify the button show to access level3
                        if (single_choice_selected_number == -1) {
                            postAnswerInterface.show_or_hide_next_level_btn(false, null, "","");
                        } else {
                            postAnswerInterface.show_or_hide_next_level_btn(true, record_item_id,((TextView)checklist_listview.getChildAt(single_choice_selected_number).findViewById(R.id.choice_text)).getText().toString(),checklist_choice_array.get(single_choice_selected_number).get("ChecklistChoiceID"));
                        }
                    } else {
                        //multi
                        record_level3_multi_choice_selected_status_array[position] = !record_level3_multi_choice_selected_status_array[position];
                        checklist_adapter.notifyDataSetChanged();
                        final StringBuffer answer_buffer = new StringBuffer();
                        for (int i = 0, length = record_level3_multi_choice_selected_status_array.length; i < length; i++) {
                            if (record_level3_multi_choice_selected_status_array[i]) {
                                answer_buffer.append(checklist_choice_array.get(i).get("ChecklistChoiceID"));
                                answer_buffer.append("|");
                            }
                        }
                        answer_String = "";
                        if (answer_buffer.length() > 1) {
                            answer_String = answer_buffer.substring(0, answer_buffer.length() - 1);
                        }
                        postAnswerInterface.store_temp_record_answer(listview_position, answer_String);
                    }
                }
            }
        });
    }

    public View getInstance() {
        return checklistView;
    }

    @Override
    public Void onPostSuccess() {
        checklist_adapter.notifyDataSetChanged();
        return null;
    }

    @Override
    public Void onPostFail() {
        return null;
    }

    @Override
    public String call_to_post() {
        return null;
    }

    @Override
    public View create_question_listview_view() {
        return null;
    }

}
