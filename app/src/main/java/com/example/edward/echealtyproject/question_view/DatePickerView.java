package com.example.edward.echealtyproject.question_view;

import android.app.Dialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.edward.echealtyproject.Global;
import com.example.edward.echealtyproject.PostAnswerActivity;
import com.example.edward.echealtyproject.R;
import com.example.edward.echealtyproject.SurveyQuestionActivity;

import java.util.HashMap;

/**
 * Created by edward on 12/17/2015.
 */
public class DatePickerView extends QuestionListviewItemView {
    PostAnswerActivity postAnswerActivity;
    HashMap<String, Object> question_hashmap;
    HashMap<String, Object> answer_hashmap;
    int TYPE;
    int listview_position;
    TextView item_content;
    View listview_item;
    Dialog dialog;

    public DatePickerView(PostAnswerActivity postAnswerActivity, HashMap<String, Object> question_hashmap, HashMap<String, Object> answer_hashmap, int TYPE, int listview_position) {
        this.postAnswerActivity = postAnswerActivity;
        this.question_hashmap = question_hashmap;
        this.answer_hashmap = answer_hashmap;
        this.TYPE = TYPE;
        this.listview_position = listview_position;
    }

    public DatePickerView(PostAnswerActivity postAnswerActivity,  boolean is_survey_mode, int listview_position) {
        this.postAnswerActivity = postAnswerActivity;
        this.question_hashmap = postAnswerActivity.get_question(listview_position);
        this.answer_hashmap = postAnswerActivity.get_answer(listview_position);
        if (is_survey_mode){
            TYPE = Global.getInstance().SURVERY_MODE;
        }else {
            TYPE = Global.getInstance().RECORD_MODE;
        }
        this.listview_position = listview_position;
    }

    @Override
    public Void onPostSuccess() {
        postAnswerActivity.reload();
        return null;
    }

    @Override
    public Void onPostFail() {
        return null;
    }

    @Override
    public String call_to_post() {
        return null;
    }

    @Override
    public View create_question_listview_view() {
        listview_item = View.inflate(postAnswerActivity, R.layout.survey_question_dropdown_item, null);
        TextView date_picker_level3_btn = (TextView) listview_item.findViewById(R.id.level3_btn);
        date_picker_level3_btn.setVisibility(View.GONE);
        TextView item_name = (TextView) listview_item.findViewById(R.id.item_name);
        item_content = (TextView) listview_item.findViewById(R.id.content);
        item_name.setText((String) question_hashmap.get("Content_ZH-CN"));


        if (TYPE == Global.SURVERY_MODE) {
            if (((String) answer_hashmap.get("Answer")).contains("00:00:00")) {
                item_content.setText(((String) answer_hashmap.get("Answer")).replace("00:00:00", ""));
            } else {
                item_content.setText((String) answer_hashmap.get("Answer"));
            }
        } else {
            item_content.setText((String) ((SurveyQuestionActivity) postAnswerActivity).getTemp_answer_array()[listview_position]);
        }

        listview_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(postAnswerActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.date_picker_dialog);
                dialog.show();
                final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker);
                Button back = (Button) dialog.findViewById(R.id.back_button);
                back.setText("返回");
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                Button finish = (Button) dialog.findViewById(R.id.finish_button);
                finish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //http post
                        System.out.println(datePicker.getYear() + "|" + datePicker.getMonth() + 1 + "|" + datePicker.getDayOfMonth());
                        final StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append(datePicker.getYear());
                        stringBuffer.append("-");
                        stringBuffer.append(datePicker.getMonth() + 1);
                        stringBuffer.append("-");
                        stringBuffer.append(datePicker.getDayOfMonth());

                        if (TYPE == Global.SURVERY_MODE) {
                            postAnswerActivity.post_answer(stringBuffer.toString(), (String) question_hashmap.get("SurveyItemID"), DatePickerView.this);
                            item_content.setText(stringBuffer.toString());
                            answer_hashmap.put("Answer", stringBuffer.toString());
                            dialog.dismiss();
                        } else {
                            //show selected date at item content, but not submit
                            item_content.setText(stringBuffer.toString());
                            postAnswerActivity.store_temp_record_answer(listview_position, stringBuffer.toString());
                            dialog.dismiss();
                        }
                    }
                });
            }
        });

        return listview_item;

    }
}
