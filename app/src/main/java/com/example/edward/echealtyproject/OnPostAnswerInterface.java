package com.example.edward.echealtyproject;

import android.view.View;

/**
 * Created by edward on 12/3/2015.
 */
public interface OnPostAnswerInterface {
    Void onPostSuccess();
    Void onPostFail();
    String call_to_post();
    View create_question_listview_view();
}
