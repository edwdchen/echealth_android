package com.example.edward.echealtyproject;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

/**
 * Created by edward on 12/17/2015.
 */
public class EditDeleteView extends LinearLayout{

    OnEditDeleteListener onEditDeleteListener;
    ImageButton edit_btn;
    ImageButton delete_btn;
    Context mContext;

    public EditDeleteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    private void initView() {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.edit_delete_btn_layout, this);

        edit_btn = (ImageButton) findViewById(R.id.edit_btn);
        delete_btn = (ImageButton) findViewById(R.id.delete_btn);

        edit_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onEditDeleteListener){
                    onEditDeleteListener.onEdit();
                }
            }
        });

        delete_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onEditDeleteListener){
                    onEditDeleteListener.onDelete();
                }
            }
        });
    }

    public OnEditDeleteListener getOnEditDeleteListener() {
        return onEditDeleteListener;
    }

    public void setOnEditDeleteListener(OnEditDeleteListener onEditDeleteListener) {
        this.onEditDeleteListener = onEditDeleteListener;
    }

}
