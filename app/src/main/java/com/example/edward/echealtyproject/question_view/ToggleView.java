package com.example.edward.echealtyproject.question_view;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.edward.echealtyproject.API_model;
import com.example.edward.echealtyproject.Custom_toggle_button;
import com.example.edward.echealtyproject.Global;
import com.example.edward.echealtyproject.OnToggleClickListen;
import com.example.edward.echealtyproject.PostAnswerActivity;
import com.example.edward.echealtyproject.R;
import com.example.edward.echealtyproject.SurveyQuestionActivity;
import com.example.edward.echealtyproject.plist.PlistHandler;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by edward on 12/16/2015.
 */
public class ToggleView extends QuestionListviewItemView {

    PostAnswerActivity postAnswerActivity;
    HashMap<String, Object> question_hashmap;
    HashMap<String, Object> answer_hashmap;
    int TYPE;
    int listview_position;
    Custom_toggle_button custom_toggle_button;
    View listview_item;

    public ToggleView(PostAnswerActivity postAnswerActivity, HashMap<String, Object> question_hashmap, HashMap<String, Object> answer_hashmap, int TYPE, int listview_position) {
        this.postAnswerActivity = postAnswerActivity;
        this.question_hashmap = question_hashmap;
        this.answer_hashmap = answer_hashmap;
        this.TYPE = TYPE;
        this.listview_position = listview_position;
    }

    public ToggleView(PostAnswerActivity postAnswerActivity, int TYPE, int listview_position) {
        this.postAnswerActivity = postAnswerActivity;
        this.question_hashmap = postAnswerActivity.get_question(listview_position);
        this.answer_hashmap = postAnswerActivity.get_answer(listview_position);
        this.TYPE = TYPE;
        this.listview_position = listview_position;
    }

    @Override
    public Void onPostSuccess() {
        postAnswerActivity.reload();
        return null;
    }

    @Override
    public Void onPostFail() {
        return null;
    }

    @Override
    public String call_to_post() {
        return null;
    }

    @Override
    public View create_question_listview_view() {
        listview_item = View.inflate(postAnswerActivity, R.layout.survey_question_togglt_item, null);
        TextView item_name = (TextView) listview_item.findViewById(R.id.item_name);
        item_name.setText((String) question_hashmap.get("Content_ZH-CN"));
        TextView description_text = (TextView) listview_item.findViewById(R.id.description_text);
        description_text.setText((String) question_hashmap.get("Description_ZH-CN"));
        final TextView toggle_level3_btn = (TextView) listview_item.findViewById(R.id.level3_btn);
        custom_toggle_button = (Custom_toggle_button) listview_item.findViewById(R.id.switch_btn);
        custom_toggle_button.setLeft_text((String) question_hashmap.get("TrueValue_ZH-CN"));
        custom_toggle_button.setRight_text((String) question_hashmap.get("FalseValue_ZH-CN"));
        custom_toggle_button.setOnToggleClickListen(new OnToggleClickListen() {
            @Override
            public void onclick(final int select_number) {

                String answer_string = null;
                if (select_number == 0) {
                    answer_string = "";
                } else if (select_number == 1) {
                    answer_string = "1";
                } else if (select_number == 2) {
                    answer_string = "0";    //******attention!!!*****
                }
                if (TYPE == Global.SURVERY_MODE) {
                    postAnswerActivity.post_answer(answer_string, (String) question_hashmap.get("SurveyItemID"), ToggleView.this);
                }else {
                    postAnswerActivity.store_temp_record_answer(listview_position,answer_string);
                }
            }
        });

        //in toggle, answer == 0 means false, answer ==1 means true, answer == null means no choice
        if (TYPE == Global.SURVERY_MODE) {

            if (answer_hashmap.get("Answer") == null || answer_hashmap.get("Answer").equals("")) {
                custom_toggle_button.setSelect_number(0);
            } else if (answer_hashmap.get("Answer").equals("1")) {
                custom_toggle_button.make_button1_selected();
            } else {
                custom_toggle_button.make_button2_selected();
            }
        }
        if ((question_hashmap.get("IsLevel3Present")).equals("1")) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("debug:" + API_model.getInstance().survey_item_id_questions(Global.getInstance().getMember_id(), (String) question_hashmap.get("SurveyItemID")));
                    final ArrayList<HashMap<String, ?>> level3_list = PlistHandler.readURLArray(API_model.getInstance().survey_item_id_questions(Global.getInstance().getMember_id(), (String) question_hashmap.get("SurveyItemID")));
                    System.out.println("level3_list:" + API_model.getInstance().survey_item_id_questions(Global.getInstance().getMember_id(), (String) question_hashmap.get("SurveyItemID")));
                    System.out.println("level3_list:" + level3_list);
                    if (level3_list.size() == 1) {
                        postAnswerActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                toggle_level3_btn.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        toggle_level3_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(postAnswerActivity, SurveyQuestionActivity.class);
                                intent.putExtra("level3", level3_list);
                                intent.putExtra("survey_item_id", (String) question_hashmap.get("SurveyItemID"));
                                postAnswerActivity.startActivity(intent);
                            }
                        });
                        postAnswerActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                toggle_level3_btn.setVisibility(View.VISIBLE);
                            }
                        });

                    }
                }
            }).start();
        } else {
            toggle_level3_btn.setVisibility(View.GONE);
        }

        return listview_item;
    }
}
