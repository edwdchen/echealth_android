package com.example.edward.echealtyproject;

/**
 * Created by edward on 12/17/2015.
 */
public interface OnEditDeleteListener {
    void onEdit();
    void onDelete();
}
