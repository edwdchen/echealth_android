package com.example.edward.echealtyproject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.edward.echealtyproject.plist.PlistHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by edward on 12/18/2015.
 */
public class RecordSummaryItemAdapter extends BaseAdapter {

    Context mContext;
    String RecordID;
    RequestQueue mQueue;
    ArrayList<ArrayList<HashMap<String, Object>>> record_answer_info;

    public RecordSummaryItemAdapter(Context mContext, String recordID, ArrayList<ArrayList<HashMap<String, Object>>> record_answer_info) {
        this.mContext = mContext;
        this.RecordID = recordID;
        this.record_answer_info = record_answer_info;
        mQueue = Volley.newRequestQueue(mContext);
    }

    public RecordSummaryItemAdapter(Context mContext, ArrayList<ArrayList<HashMap<String, Object>>> record_answer_info) {
        this.mContext = mContext;
        this.record_answer_info = record_answer_info;
        System.out.println("record_answer_info:" + record_answer_info);
    }

    @Override
    public int getCount() {
        return record_answer_info.size();
    }

    @Override
    public Object getItem(int position) {
        return record_answer_info.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ArrayList<HashMap<String, Object>> single_attampt_info = record_answer_info.get(position);

        View item_view = View.inflate(mContext, R.layout.record_summary_item_layout, null);
        EditDeleteView editDeleteView = (EditDeleteView) item_view.findViewById(R.id.edit_delete_btn);

        editDeleteView.setOnEditDeleteListener(new OnEditDeleteListener() {
            @Override
            public void onEdit() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<HashMap<String, Object>> level3_info = PlistHandler.readURLArray(API_model.getInstance().record_level3_questions(Global.getInstance().getMember_id(), RecordID));
                        System.out.println("level3:" + level3_info);
                        if (level3_info.size() == 1) {
                            //only the last edit info
                        } else {
                            ActivityUtil.getInstance().enter_record_level3(mContext, level3_info, (String) single_attampt_info.get(0).get("AnswerContent_ZHCN"), RecordID,single_attampt_info.get(single_attampt_info.size()-1).get("Attempt").toString());
                        }
                    }
                }).start();
            }

            @Override
            public void onDelete() {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, API_model.getInstance().record_delete_answer(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (response.replaceAll("\\s+", "").equals("1000")) {
                                    //problem in api return, an extra space is added in front of the result
                                    Toast.makeText(mContext, "Submission success", Toast.LENGTH_SHORT).show();
                                    record_answer_info.remove(position);
                                    notifyDataSetChanged();
                                } else {
                                    Toast.makeText(mContext, "Submission fail", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(mContext, "Network error", Toast.LENGTH_SHORT).show();
                    }
                }) {
                    protected Map<String, String> getParams() {
                        //在这里设置需要post的参数
                        HashMap<String, String> post_map = new HashMap<String, String>();
                        post_map.put("member_id", Global.getInstance().getMember_id());
                        post_map.put("record_id", RecordID);
                        post_map.put("attempt", (String) single_attampt_info.get(single_attampt_info.size() - 1).get("Attempt"));
                        System.out.println("post_map:" + post_map);
                        return post_map;
                    }
                };

                mQueue.add(stringRequest);
            }
        });

        LinearLayout linearLayout = (LinearLayout) item_view.findViewById(R.id.record_summary_item);

        for (int i = 0, length = single_attampt_info.size() - 1; i < length; i++) {

            if (i == 0) {
                TextView sub_title = new TextView(mContext);
                sub_title.setTextAppearance(mContext, android.R.style.TextAppearance_Medium);
                sub_title.setTypeface(null, Typeface.BOLD);

                if (((String) single_attampt_info.get(0).get("AnswerContent_ZHCN")).contains("|")) {
                    sub_title.setText(((String) single_attampt_info.get(0).get("AnswerContent_ZHCN")).replace("|", ""));
                } else {
                    sub_title.setText((String) single_attampt_info.get(0).get("AnswerContent_ZHCN"));
                }
                linearLayout.addView(sub_title);

            } else if ((((String) single_attampt_info.get(i).get("IsHeader")).equals("1"))) {
                TextView header = new TextView(mContext);
                header.setText((String) single_attampt_info.get(i).get("QuestionContent_ZHCN"));
                header.setTextAppearance(mContext, android.R.style.TextAppearance_Medium);
                header.setTypeface(null, Typeface.BOLD);
                linearLayout.addView(header);
            } else {
                RecordQuestionAnswerPairView recordQuestionAnswerPairView = new RecordQuestionAnswerPairView(mContext, single_attampt_info.get(i));
                linearLayout.addView(recordQuestionAnswerPairView);
            }
        }

        return item_view;
    }
}
