package com.example.edward.echealtyproject.question_view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.edward.echealtyproject.API_model;
import com.example.edward.echealtyproject.Global;
import com.example.edward.echealtyproject.MultiChoiceAdapter;
import com.example.edward.echealtyproject.OnPostAnswerInterface;
import com.example.edward.echealtyproject.PostAnswerActivity;
import com.example.edward.echealtyproject.PostAnswerInterface;
import com.example.edward.echealtyproject.R;
import com.example.edward.echealtyproject.SingleChoiceAdapter;
import com.example.edward.echealtyproject.SurveyQuestionActivity;
import com.example.edward.echealtyproject.plist.PlistHandler;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by edward on 12/3/2015.
 */
public class DropdownMenuView implements OnPostAnswerInterface {
    ArrayList<String> selected_choice_id_arraylist = new ArrayList<>();
    int question_list_position;
    Dialog dialog;
    String answer_String;
    Context mContext;
    View dropdownmenuView;
    TextView item_name, item_content;
    boolean survey_mode;
    int Type = 1;//1:single 2:multi
    String survey_item_id;
    String record_item_id;
    SingleChoiceAdapter singleChoiceAdapter;
    MultiChoiceAdapter multiChoiceAdapter;
    int single_choice_selected_number = -1;
    boolean[] choice_checked_array;// used to store selected choices in dialog listview

    ArrayList<HashMap<String, String>> choice_list;

    HashMap<String, ?> question_hashmap, answer_hashmap;
    //question:http://echealth.iclassu.com/ios/survey_questions.php?survey_id=24&member_id=2
    //answer sample:http://echealth.iclassu.com/ios/survey_answers.php?survey_id=24&member_id=2
    ArrayList<HashMap<String, String>> dropdownmenu_choice_array;
    ArrayList<HashMap<String, String>> answer_arraylist;
    PostAnswerInterface postAnswerInterface;

    int single_selected_position = -1;

    public DropdownMenuView(Context context, HashMap<String, ?> question_hashmap, HashMap<String, ?> answer_hashmap, boolean survey_mode, PostAnswerInterface postAnswerInterface, int question_list_position, ArrayList<String> selected_choice_id_arraylist) {

        mContext = context;
        this.survey_mode = survey_mode;
        this.question_hashmap = question_hashmap;
        this.answer_hashmap = answer_hashmap;
        if (selected_choice_id_arraylist != null) {
            this.selected_choice_id_arraylist = selected_choice_id_arraylist;
        }
        this.question_list_position = question_list_position;
        Type = Integer.parseInt((String) question_hashmap.get("Type"));
        if (survey_mode) {
            answer_arraylist = (ArrayList<HashMap<String, String>>) answer_hashmap.get("Answer");
            survey_item_id = (String) question_hashmap.get("SurveyItemID");
        } else {
            record_item_id = (String) question_hashmap.get("RecordItemID");
        }

        this.postAnswerInterface = postAnswerInterface;

        initView();
    }

    public DropdownMenuView(PostAnswerActivity postAnswerActivity, boolean survey_mode, int position) {

        mContext = postAnswerActivity;
        this.survey_mode = survey_mode;
        this.question_hashmap = postAnswerActivity.get_question(position);
        this.answer_hashmap = postAnswerActivity.get_answer(position);
        if (selected_choice_id_arraylist != null) {
            this.selected_choice_id_arraylist = selected_choice_id_arraylist;
        }
        this.question_list_position = position;
        Type = Integer.parseInt((String) question_hashmap.get("Type"));
        if (survey_mode) {
            answer_arraylist = (ArrayList<HashMap<String, String>>) answer_hashmap.get("Answer");
            survey_item_id = (String) question_hashmap.get("SurveyItemID");
        } else {
            record_item_id = (String) question_hashmap.get("RecordItemID");
        }

        this.postAnswerInterface = postAnswerActivity;

        initView();
    }

    private void initView() {
        dropdownmenuView = View.inflate(mContext, R.layout.survey_question_dropdown_item, null);
        choice_list = (ArrayList<HashMap<String, String>>) question_hashmap.get("ChoiceArray");
        final TextView DROP_DOWN_MENU_level3_btn = (TextView) dropdownmenuView.findViewById(R.id.level3_btn);
        item_name = (TextView) dropdownmenuView.findViewById(R.id.item_name);
        item_content = (TextView) dropdownmenuView.findViewById(R.id.content);
        item_name.setText((String) question_hashmap.get("Content_ZH-CN"));
        final ArrayList<HashMap<String, String>> choice_array = (ArrayList<HashMap<String, String>>) question_hashmap.get("ChoiceArray");
        if (Type == 1) {
            singleChoiceAdapter = new SingleChoiceAdapter(mContext, choice_list);
        } else {
            multiChoiceAdapter = new MultiChoiceAdapter(mContext, choice_list);
            //answer_array is used to store which choices are checked
            choice_checked_array = new boolean[((ArrayList<Object>) question_hashmap.get("ChoiceArray")).size()];
        }

        if (survey_mode) {
            final ArrayList<HashMap<String, String>> answer_array = (ArrayList<HashMap<String, String>>) answer_hashmap.get("Answer");
            String display_content = "";

            for (int i = 0, length = answer_array.size(); i < length; i++) {
                if (answer_array.get(i).get("IsChecked").equals("1")) {
//                    dropdownmenuView.setTag(i);
                    if (Type==1){
                    single_selected_position = i;

                    }else {
                        choice_checked_array[i]= true;
                    }
                    display_content = display_content + choice_array.get(i).get("Content_ZH-CN") + ",";
                }
            }

            if (display_content.length() > 0) {
                item_content.setText(display_content.substring(0, display_content.length() - 1));
            } else {
                item_content.setText(display_content);
            }

            if (((String) question_hashmap.get("IsLevel3Present")).equals("1") && !display_content.equals("")) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final ArrayList<HashMap<String, ?>> level3_list = PlistHandler.readURLArray(API_model.getInstance().survey_item_id_questions(Global.getInstance().getMember_id(), (String) question_hashmap.get("SurveyItemID")));
                        if (level3_list.size() <= 1) {
                            //only LastEdit but no information
                            ((AppCompatActivity) mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    DROP_DOWN_MENU_level3_btn.setVisibility(View.GONE);
                                }
                            });
                        } else {
                            ((AppCompatActivity) mContext).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    DROP_DOWN_MENU_level3_btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(mContext, SurveyQuestionActivity.class);
                                            intent.putExtra("level3", level3_list);
                                            intent.putExtra("survey_item_id", (String) question_hashmap.get("SurveyItemID"));
                                            mContext.startActivity(intent);
                                        }
                                    });
                                }
                            });
                        }
                    }
                }).start();
            } else {
                DROP_DOWN_MENU_level3_btn.setVisibility(View.GONE);
            }
        } else {
            //record mode

            //initiate an boolean array with all "false" because type 2 suppose have empty answers
            if (Type == 2) {
                choice_checked_array = new boolean[((ArrayList<Object>) question_hashmap.get("ChoiceArray")).size()];
            }
            //show item content
            String content_string = "";

            if (selected_choice_id_arraylist.size() > 0) {
                for (int i = 0, length = choice_array.size(); i < length; i++) {
                    if (selected_choice_id_arraylist.contains((String) choice_array.get(i).get("DropdownChoiceID"))) {
                        content_string = content_string + "," + choice_array.get(i).get("Content_ZH-CN");
                        if (Type == 2) {
                            choice_checked_array[i] = true;
                        } else {
                            singleChoiceAdapter.setSelect_item_position(i);
                        }
                    }
                }
                item_content.setText(content_string.substring(1));
            }
            DROP_DOWN_MENU_level3_btn.setVisibility(View.GONE);
        }

        dropdownmenuView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                choice_list = (ArrayList<HashMap<String, String>>) question_hashmap.get("ChoiceArray");
                dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.drop_down_menu_layout);

                Button back = (Button) dialog.findViewById(R.id.back_button);
                back.setText("返回");
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                TextView dropdown_info = (TextView) dialog.findViewById(R.id.dropdown_info);
                ListView listview = (ListView) dialog.findViewById(R.id.dropdown_listview);
                if (Type == 1) {

                    //single choice
                    dropdown_info.setText("你只可以选择其中一项。");
//                    singleChoiceAdapter = new SingleChoiceAdapter(mContext, choice_list);
                    listview.setAdapter(singleChoiceAdapter);
                    if (single_selected_position >=0) {
                        singleChoiceAdapter.setSelect_item_position(single_selected_position);
                        singleChoiceAdapter.notifyDataSetChanged();
                    }
                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            if (position != singleChoiceAdapter.getSelect_item_position()) {
                                singleChoiceAdapter.setSelect_item_position(position);
                                singleChoiceAdapter.notifyDataSetChanged();
                            } else {
                                //click the same position means user wants to erase the answer
                                single_selected_position =-1;
                                singleChoiceAdapter.setSelect_item_position(-1);
                                singleChoiceAdapter.notifyDataSetChanged();
                            }
                        }
                    });
                } else {

                    //multi choice
                    dropdown_info.setText("你可以选择多于一项。");
//                    multiChoiceAdapter = new MultiChoiceAdapter(mContext, choice_list);
                    listview.setAdapter(multiChoiceAdapter);

                    //show selected choices
                    if (survey_mode) {
                        for (int i = 0;i<choice_checked_array.length;i++){
                            if (choice_checked_array[i]){
                                multiChoiceAdapter.change_status(i);
                            }
                        }
                    } else {
                        if (selected_choice_id_arraylist.size() > 0) {
                            for (int i = 0, length = choice_array.size(); i < length; i++) {
                                if (selected_choice_id_arraylist.contains((String) choice_array.get(i).get("DropdownChoiceID"))) {
                                    multiChoiceAdapter.change_status(i);
                                }
                            }
                        }
                    }

                    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            multiChoiceAdapter.change_status(position);
                            choice_checked_array[position] = !choice_checked_array[position];
                        }
                    });
                }

                Button finish = (Button) dialog.findViewById(R.id.finish_button);
                finish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //http post
                        //answer = getSelectedPosition + 1
                        if (Type == 1) {
                            if (survey_mode) {

                                if (singleChoiceAdapter.getSelect_item_position() == -1) {
                                    postAnswerInterface.post_answer("", survey_item_id, DropdownMenuView.this);
                                } else {
                                    postAnswerInterface.post_answer(choice_list.get(singleChoiceAdapter.getSelect_item_position()).get("DropdownChoiceID"), survey_item_id, DropdownMenuView.this);
                                }
                            } else {
                                //record mode
                                //store selected choice id in an arraylist

                                selected_choice_id_arraylist.clear();
                                if (singleChoiceAdapter.getSelect_item_position() != -1) {

                                    selected_choice_id_arraylist.add(choice_list.get(singleChoiceAdapter.getSelect_item_position()).get("DropdownChoiceID"));
                                }

                                if (singleChoiceAdapter.getSelect_item_position() == -1) {
                                    item_content.setText("");
                                } else {
                                    item_content.setText(choice_list.get(singleChoiceAdapter.getSelect_item_position()).get("Content_ZH-CN"));
                                }

                                postAnswerInterface.store_temp_record_answer(question_list_position, selected_choice_id_arraylist);
                            }
                        } else {

                            if (survey_mode) {

                                String post_content_string = "";

                                for (int i = 0;i<choice_checked_array.length;i++){
                                    if (choice_checked_array[i]){
                                        post_content_string = post_content_string + choice_list.get(i).get("DropdownChoiceID")+"|";
                                    }
                                }

                                if (post_content_string.length()>0){
                                    post_content_string = post_content_string.substring(0,post_content_string.length()-1);
                                }

                                System.out.println("post_content_string:"+post_content_string);
                                postAnswerInterface.post_answer(post_content_string, survey_item_id, DropdownMenuView.this);

                            } else {

                                //show selected items on item content
                                int i = 0;
                                String content_string = "";
                                selected_choice_id_arraylist.clear();
                                while (i < choice_checked_array.length) {
                                    if (choice_checked_array[i]) {
                                        content_string = content_string + "," + choice_array.get(i).get("Content_ZH-CN");
                                        selected_choice_id_arraylist.add(choice_list.get(i).get("DropdownChoiceID"));
                                    }
                                    i++;
                                }
                                postAnswerInterface.store_temp_record_answer(question_list_position, selected_choice_id_arraylist);

                                if (content_string.length() > 0) {
                                    item_content.setText(content_string.substring(1, content_string.length()));
                                } else {
                                    item_content.setText("");
                                }

                                //put selected choices into record temp_answer_array

                            }
                        }
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    public View getInstance() {
        return dropdownmenuView;
    }

    @Override
    public Void onPostSuccess() {
        postAnswerInterface.reload();
        dialog.dismiss();
        return null;
    }

    @Override
    public Void onPostFail() {
        return null;
    }

    public String call_to_post() {
        if (Type == 1) {
            if (singleChoiceAdapter.getSelect_item_position() == -1) {
                postAnswerInterface.post_answer("", survey_item_id, DropdownMenuView.this);
            } else {
                postAnswerInterface.post_answer(choice_list.get(singleChoiceAdapter.getSelect_item_position()).get("DropdownChoiceID"), survey_item_id, DropdownMenuView.this);
            }
        } else {
            return multiChoiceAdapter.get_answer_string();
        }
        return "";
    }

    @Override
    public View create_question_listview_view() {
        return null;
    }
}
