/*
 * 2014/10/9
 * Wong Kwan
 * 
 */

package com.example.edward.echealtyproject.plist;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class PlistHandler {
	static int timeoutSocket = 10000;

	public static boolean downloadToFile(String url, File file){
		String result = null;
		
		HttpGet request = new HttpGet(url);
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 3000;
//		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//
//		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

		try {
			HttpResponse response = new DefaultHttpClient(httpParameters).execute(request);
			if(response.getStatusLine().getStatusCode() == 200){
				result = EntityUtils.toString(response.getEntity());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		//System.out.println(result);
		
		//write the login information into a plist file
        if(file.exists()){
            file.delete();
        }
		FileOutputStream output;
		try {
			output = new FileOutputStream(file);
			output.write(result.getBytes());
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static HashMap<?,?> readFile(String str){
		String result = null;
		
		//read the plist file
		StringBuffer fileData = new StringBuffer(1024);
		FileReader file = null;
		BufferedReader br = null;
		
		try{
			file = new FileReader(str);
			result = null;
			br = new BufferedReader(file);
			while ((result = br.readLine()) != null) fileData.append(result);
		} catch (Exception e){
			e.printStackTrace();
			return null;
		} finally {
			try {
				file.close();
				br.close();
			} catch (Exception ex){
				ex.printStackTrace();
				return null;
			}
		}
		
		//put the plist into Hashmap
		HashMap<?,?> hashmap = null;
		try {
			hashmap = (HashMap<?,?>)Plist.objectFromXml(fileData.toString());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return hashmap;
		
	}
	
	public static ArrayList readFileArray(File f){
		String result = null;
		
		//read the plist file
		StringBuffer fileData = new StringBuffer(1024);
		FileReader file = null;
		BufferedReader br = null;
		
		try{
			file = new FileReader(f);
			result = null;
			br = new BufferedReader(file);
			while ((result = br.readLine()) != null) fileData.append(result+"\n");
		} catch (Exception e){
			e.printStackTrace();
			return null;
		} finally {
			try {
				file.close();
				br.close();
			} catch (Exception ex){
				ex.printStackTrace();
				return null;
			}
		}
		
		//put the plist into Hashmap
		ArrayList list = null;
		try {
			list = (ArrayList)Plist.objectFromXml(fileData.toString());
		} catch (XmlParseException e) {
			e.printStackTrace();
			return null;
		}
		
		return list;
		
	}
	
	static HashMap<?,?> readFile(File f){
		String result = null;
		
		//read the plist file
		StringBuffer fileData = new StringBuffer(1024);
		FileReader file = null;
		BufferedReader br = null;
		
		try{
			file = new FileReader(f);
			result = null;
			br = new BufferedReader(file);
			while ((result = br.readLine()) != null) fileData.append(result);
		} catch (Exception e){
			e.printStackTrace();
			return null;
		} finally {
			try {
				file.close();
				br.close();
			} catch (Exception ex){
				ex.printStackTrace();
				return null;
			}
		}
		
		//put the plist into Hashmap
		HashMap<?,?> hashmap = null;
		try {
			hashmap = (HashMap<?,?>)Plist.objectFromXml(fileData.toString());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return hashmap;
		
	}
	
	public static HashMap<?,?> readURL(String url){
		String result = null;
		
		//read the plist file
		StringBuffer fileData = new StringBuffer(1024);
		BufferedReader br = null;
		
		try{
			URL oracle = new URL(url);
			for(int i = 0 ; i < 10 && br == null; i++) br = new BufferedReader(new InputStreamReader(oracle.openStream()));
			while ((result = br.readLine()) != null) fileData.append(result);
		} catch (Exception e){
			e.printStackTrace();
			return null;
		} finally {
			try {
				br.close();
			} catch (Exception ex){
				ex.printStackTrace();
				return null;
			}
		}
		
		//put the plist into Hashmap
		HashMap<?,?> hashmap = null;
		try {
			hashmap = (HashMap<?,?>)Plist.objectFromXml(fileData.toString());
		} catch (XmlParseException e) {
			e.printStackTrace();
			return null;
		}
		
		return hashmap;
		
	}
	
	public static ArrayList readURLArray(String url){
		String result = null;
		
		//read the plist file
		StringBuffer fileData = new StringBuffer(1024);
		BufferedReader br = null;
		
		try{
			URL oracle = new URL(url);
			for(int i = 0 ; i < 10 && br == null ;i++) br = new BufferedReader(new InputStreamReader(oracle.openStream()));
			while ((result = br.readLine()) != null) fileData.append(result+"\n");
		} catch (Exception e){
			e.printStackTrace();
			return null;
		} finally {
			try {
				br.close();
			} catch (Exception ex){
				ex.printStackTrace();
				return null;
			}
		}
		
		//put the plist into Hashmap
		ArrayList list = null;
		try {
			list = (ArrayList)Plist.objectFromXml(fileData.toString());
		} catch (XmlParseException e) {
			e.printStackTrace();
			return null;
		}
		
		return list;
		
	}
}
