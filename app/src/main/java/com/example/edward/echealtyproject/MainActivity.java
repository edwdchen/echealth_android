package com.example.edward.echealtyproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.edward.echealtyproject.plist.Plist;
import com.example.edward.echealtyproject.plist.XmlParseException;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    final String TAG = "ECHEALTH";
    EditText user_name_input,user_password_input;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button login,register,statement,thanks;
        user_name_input = (EditText) findViewById(R.id.name_input);
        user_password_input = (EditText) findViewById(R.id.password_input);
        login = (Button) findViewById(R.id.login);
        register = (Button) findViewById(R.id.register);
        statement = (Button) findViewById(R.id.statement);
        thanks = (Button) findViewById(R.id.thanks);

        SharedPreferences sharedPreferences = getSharedPreferences("echealth", 0);
        String name = sharedPreferences.getString("user_name","");
        String password = sharedPreferences.getString("password","");

        if (name.length()==0||password.length()==0){

        }else {
            user_name_input.setText(name);
            user_password_input.setText(password);
        }

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });

        thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ThanksActivity.class);
                intent.putExtra("type",0);
                startActivity(intent);
            }
        });

        statement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,DisclaimerActivity.class);
                startActivity(intent);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = user_name_input.getText().toString();
                String password = user_password_input.getText().toString();

                if (name.length()==0 || password.length()==0){
                    Toast.makeText(MainActivity.this,"请输入账户及密码。",Toast.LENGTH_SHORT).show();
                }else {
                    Log.i(TAG, API_model.getInstance().login(name, password));
                    RequestQueue mQueue = Volley.newRequestQueue(MainActivity.this);
                    StringRequest stringRequest = new StringRequest(API_model.getInstance().login(name,password),
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d(TAG, response);
                                    try {
                                        HashMap<String,String> login_info = (HashMap<String, String>) Plist.objectFromXml(response);
                                        if (login_info.get("Success").equals("1000")){
                                            Global.getInstance().setUser_name(login_info.get("Username"));
                                            Global.getInstance().setPassword(login_info.get("Password"));
                                            Global.getInstance().setMember_id(login_info.get("MemberID"));

                                            SharedPreferences sharedPreferences = getSharedPreferences("echealth", 0);
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.putString("user_name",login_info.get("Username"));
                                            editor.putString("password",login_info.get("Password"));
                                            editor.putString("member_id",login_info.get("MemberID"));
                                            editor.commit();

                                            Intent intent = new Intent(MainActivity.this,NavigationActivity.class);
                                            startActivity(intent);
                                        }else {
                                            Toast.makeText(MainActivity.this,"我们的资料库没有此账户，请重新输入。",Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (XmlParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(TAG, error.getMessage(), error);
                        }
                    });

                    mQueue.add(stringRequest);
                }
            }
        });
    }
}
