package com.example.edward.echealtyproject.question_view;

import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.edward.echealtyproject.API_model;
import com.example.edward.echealtyproject.Global;
import com.example.edward.echealtyproject.PostAnswerActivity;
import com.example.edward.echealtyproject.R;
import com.example.edward.echealtyproject.SurveyQuestionActivity;
import com.example.edward.echealtyproject.WheelView.WheelView;
import com.example.edward.echealtyproject.plist.PlistHandler;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by edward on 12/15/2015.
 */
public class PickerView extends QuestionListviewItemView {

    String display_content;
    TextView item_content;
    Dialog dialog;
    ArrayList<String>[] pickers_array;
    int listview_position;
    PostAnswerActivity postAnswerActivity;
    HashMap<String, Object> question_hashmap;
    HashMap<String, Object> answer_hashmap;
    int TYPE;
    View listview_item;
    TextView picker_level3_btn;
    TextView dialog_textview;
    ArrayList<WheelView> wheelViewArrayList = new ArrayList<>();
    ArrayList<HashMap<String, String>> picker_choice_array;
    int Segments;

    public PickerView(PostAnswerActivity postAnswerActivity, HashMap<String, Object> question_hashmap, HashMap<String, Object> answer_hashmap, int TYPE, int listview_position) {
        this.postAnswerActivity = postAnswerActivity;
        this.question_hashmap = question_hashmap;
        this.answer_hashmap = answer_hashmap;
        this.TYPE = TYPE;
        this.listview_position = listview_position;
    }

    public PickerView(PostAnswerActivity postAnswerActivity, int TYPE, int listview_position) {
        this.postAnswerActivity = postAnswerActivity;
        this.question_hashmap = postAnswerActivity.get_question(listview_position);
        this.answer_hashmap = postAnswerActivity.get_answer(listview_position);
        this.TYPE = TYPE;
        this.listview_position = listview_position;
    }

    @Override
    public Void onPostSuccess() {
        item_content.setText(dialog_textview.getText().toString());
        dialog.dismiss();
        postAnswerActivity.reload();
        return null;
    }

    @Override
    public Void onPostFail() {
        return null;
    }

    @Override
    public String call_to_post() {
        return null;
    }

    @Override
    public View create_question_listview_view() {
        listview_item = View.inflate(postAnswerActivity, R.layout.survey_question_dropdown_item, null);

        TextView item_name = (TextView) listview_item.findViewById(R.id.item_name);
        item_name.setText((String) question_hashmap.get("Content_ZH-CN"));
        item_content = (TextView) listview_item.findViewById(R.id.content);
        TextView picker_description = (TextView) listview_item.findViewById(R.id.description_text);
        picker_level3_btn = (TextView) listview_item.findViewById(R.id.level3_btn);
        if (null != question_hashmap.get("Description_ZH-CN") && ((String) question_hashmap.get("Description_ZH-CN")).length() > 0) {
            picker_description.setText((String) question_hashmap.get("Description_ZH-CN"));
        }
        Segments = Integer.parseInt((String) question_hashmap.get("Segments"));
        pickers_array = new ArrayList[Segments];

        picker_choice_array = (ArrayList<HashMap<String, String>>) question_hashmap.get("ChoiceArray");
        for (int i = 0; i < Segments; i++) {
            HashMap<String, String> picker_choice_hashmap = picker_choice_array.get(i);
            ArrayList<String> choice_arraylist = new ArrayList<>();
            int MinValue = Integer.parseInt(picker_choice_hashmap.get("MinValue"));
            int MaxValue = Integer.parseInt(picker_choice_hashmap.get("MaxValue"));
//                          int Segment = Integer.parseInt(picker_choice_hashmap.get("Segment"));
            for (int j = MinValue; j <= MaxValue; j++) {
                choice_arraylist.add(String.valueOf(j));
            }
            pickers_array[i] = choice_arraylist;
        }

        listview_item.setTag(pickers_array);//used in onitemclicklistener
        if (TYPE == Global.RECORD_MODE) {
            picker_level3_btn.setVisibility(View.GONE);
        } else {

            if (answer_hashmap.get("Answer") == null || answer_hashmap.get("Answer").equals("")) {
                //no selected answer
                picker_level3_btn.setVisibility(View.GONE);
            } else {

                String content_string = "";

                for (int i = 0; i < Segments; i++) {
                    HashMap<String, String> picker_choice_hashmap = picker_choice_array.get(i);
                    String label_string = picker_choice_hashmap.get("Label_ZH-CN");
                    ArrayList<HashMap<String, String>> answer_arraylist = (ArrayList<HashMap<String, String>>) answer_hashmap.get("Answer");
                    String number;
                    if (answer_arraylist.size() == 0) {
                        //no selected answer
                        number = "0";
                        picker_level3_btn.setVisibility(View.GONE);
                    } else {
                        HashMap<String, String> answer_info = ((ArrayList<HashMap<String, String>>) answer_hashmap.get("Answer")).get(i);
                        number = answer_info.get("SelectedValue");
                        item_content.setText(number + label_string);
                        content_string = content_string + number + label_string;
                    }
                }
                display_content = content_string;
                item_content.setText(content_string);
            }
        }

        if (((String) question_hashmap.get("IsLevel3Present")).equals("1")) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final ArrayList<HashMap<String, ?>> level3_list = PlistHandler.readURLArray(API_model.getInstance().survey_item_id_questions(Global.getInstance().getMember_id(), (String) question_hashmap.get("SurveyItemID")));
                    if (level3_list.size() == 1) {
                        postAnswerActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                picker_level3_btn.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        picker_level3_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(postAnswerActivity, SurveyQuestionActivity.class);
                                intent.putExtra("level3", level3_list);
                                intent.putExtra("survey_item_id", (String) question_hashmap.get("SurveyItemID"));
                                postAnswerActivity.startActivity(intent);
                            }
                        });
                    }
                }
            }).start();
        } else {
            picker_level3_btn.setVisibility(View.GONE);
        }


        listview_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(postAnswerActivity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.picker_dialog_layout);
                LinearLayout wheelview_container = (LinearLayout) dialog.findViewById(R.id.wheelview_container);
                dialog_textview = (TextView) dialog.findViewById(R.id.picker_info);
                dialog_textview.setText(display_content);
                Button back = (Button) dialog.findViewById(R.id.back_button);
                back.setText("返回");
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(0,
                        LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);

                for (int i = 0; i < pickers_array.length; i++) {
                    ArrayList<String> single_wheelview_data = pickers_array[i];
                    System.out.println("single_wheelview_data:" + single_wheelview_data);
                    WheelView wheelView = new WheelView(postAnswerActivity);
                    wheelview_container.addView(wheelView);
                    wheelView.setOffset(2);
                    wheelView.setItems(single_wheelview_data);
                    wheelView.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                        @Override
                        public void onSelected(int selectedIndex, String item) {
                            refresh_dialog_picker_info();
                        }
                    });
                    wheelViewArrayList.add(wheelView);
                    wheelView.setLayoutParams(param);
                }
                dialog.show();

                Button finish = (Button) dialog.findViewById(R.id.finish_button);
                finish.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //http post
                        //answer = getSelectedPosition + 1
                        //in API, 0 = no selected item
                        //call to post
                        //example:
                        // pickerChoiceID:content;pickerChoiceID:content;pickerChoiceID:content;
                        String answer_string = "";
                        for (int i = 0; i < Segments; i++) {
                            answer_string = answer_string + picker_choice_array.get(i).get("PickerChoiceID") + ":" + wheelViewArrayList.get(i).getSeletedItem() + ";";
                        }

                        display_content = dialog_textview.getText().toString();
                        if (TYPE == Global.SURVERY_MODE) {
                            postAnswerActivity.post_answer(answer_string, (String) question_hashmap.get("SurveyItemID"), PickerView.this);
                        } else {
                            item_content.setText(dialog_textview.getText().toString());
                            dialog.dismiss();
                            postAnswerActivity.store_temp_record_answer(listview_position, answer_string);
                        }
                    }
                });
            }
        });

        return listview_item;
    }

    private void refresh_dialog_picker_info() {
        String info_string = "";
        for (int i = 0; i < Segments; i++) {
            HashMap<String, String> picker_choice_hashmap = picker_choice_array.get(i);
            info_string = info_string + wheelViewArrayList.get(i).getSeletedItem() + picker_choice_hashmap.get("Label_ZH-CN");
        }
        dialog_textview.setText(info_string);
    }
}
