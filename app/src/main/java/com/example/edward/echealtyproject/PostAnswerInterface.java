package com.example.edward.echealtyproject;

import java.util.HashMap;

/**
 * Created by edward on 12/3/2015.
 */
public interface PostAnswerInterface {
    void post_answer(String answer_string,String item_id,OnPostAnswerInterface onPostAnswerInterface);
    //interface to post individual answer
    void show_or_hide_next_level_btn(boolean is_show,String level2_record_id,String level3_title,String checklist_choice);//only used in record level2
    void reload();//reload the whole activity
    void store_temp_record_answer(int question_list_position,Object answer_object);//store temp answers in case of recycling
    HashMap<String,Object> get_question(int pos);
    HashMap<String,Object> get_answer(int pos);
}