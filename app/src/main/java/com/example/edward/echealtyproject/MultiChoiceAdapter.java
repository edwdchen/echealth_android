package com.example.edward.echealtyproject;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by edward on 11/23/2015.
 */
public class MultiChoiceAdapter extends BaseAdapter{

    private int select_item_position = -1;
    ArrayList<HashMap<String,String>> choice_list;
    ArrayList<Boolean> selected_status_list = new ArrayList<>();
    Context context;

    public MultiChoiceAdapter(Context context, ArrayList<HashMap<String, String>> choice_list) {
        this.choice_list = choice_list;
        this.context = context;
        for (int i =0,length = choice_list.size();i<length;i++){
            selected_status_list.add(false);
        }
    }

    @Override
    public int getCount() {
        return choice_list.size();
    }

    @Override
    public HashMap<String,String> getItem(int position) {
        return choice_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = View.inflate(context,R.layout.choice_item,null);
        ImageView choice_button = (ImageView) itemView.findViewById(R.id.choice_button);
        TextView choice_text = (TextView) itemView.findViewById(R.id.choice_text);
        choice_text.setText(choice_list.get(position).get("Content_ZH-CN"));
        if (selected_status_list.get(position)){
            choice_button.setImageResource(R.mipmap.check_box);
        }else {
            choice_button.setImageResource(R.mipmap.check_box_empty);
        }
        return itemView;
    }

    public void change_status(int position){
        selected_status_list.set(position,!selected_status_list.get(position));
        notifyDataSetChanged();
    }

    public String get_answer_string(){
        StringBuffer stringBuffer = new StringBuffer();
        for (int i=0,length = selected_status_list.size();i<length;i++){
            if (selected_status_list.get(i)){
                stringBuffer.append((String) choice_list.get(i).get("DropdownChoiceID"));
                stringBuffer.append("|");
            }else {
                stringBuffer.append("0");
            }
            if (stringBuffer.length()>0){
               return stringBuffer.subSequence(0,stringBuffer.length()).toString();
            }else {
                return "";
            }
        }
        return  stringBuffer.toString();
    }
}
