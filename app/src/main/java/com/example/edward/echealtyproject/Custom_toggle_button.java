package com.example.edward.echealtyproject;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by edward on 11/17/2015.
 */
public class Custom_toggle_button extends LinearLayout {
    TextView button1, button2;
    Context context;

    public void setOnToggleClickListen(com.example.edward.echealtyproject.OnToggleClickListen onToggleClickListen) {
        this.onToggleClickListen = onToggleClickListen;
    }

    OnToggleClickListen onToggleClickListen;

    public int getSelect_number() {
        return select_number;
    }

    private int normal_color = Color.BLACK;

    private int highlight_color = Color.WHITE;

    public String getLeft_text() {

        return left_text;
    }

    public void setLeft_text(String left_text) {
        this.left_text = left_text;
        button1.setText(left_text);
    }

    public String getRight_text() {
        return right_text;
    }

    public void setRight_text(String right_text) {
        this.right_text = right_text;
        button2.setText(right_text);
    }

    private String left_text = "ON", right_text = "OFF";

    private int highlight_text_color = Color.WHITE;

    public void setSelect_number(int select_number) {
        this.select_number = select_number;
    }

    private int select_number = 0;//0=none, 1= first, 2 = second

    public Custom_toggle_button(final Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.Custom_toggle_button);

        highlight_color = a.getColor(R.styleable.Custom_toggle_button_highlight_color,
                Color.WHITE);

//        normal_color  =a.getColor(R.styleable.Custom_toggle_button_normal_color,);

        left_text = a.getString(R.styleable.Custom_toggle_button_left_text);

        right_text = a.getString(R.styleable.Custom_toggle_button_right_text);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.round_cornor_toggle_button, this);
        LinearLayout container = (LinearLayout) findViewById(R.id.container);
        Drawable background = container.getBackground();
        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable) background).getPaint().setColor(highlight_color);
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable) background).setStroke(2, highlight_color);
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable) background).setColor(highlight_color);
        }

        button1 = (TextView) findViewById(R.id.button1);
        button2 = (TextView) findViewById(R.id.button2);
        button1.setText(left_text);
        button2.setText(right_text);

        button1.setTextColor(highlight_color);
        button2.setTextColor(highlight_color);

        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select_number == 1) {
                    select_number = 0;
                    button1.setTextColor(highlight_color);
                    button1.setBackgroundColor(Color.TRANSPARENT);
                } else {
                    make_button1_selected();
                }
                if (null != onToggleClickListen) {

                    onToggleClickListen.onclick(select_number);
                }
            }
        });

        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select_number == 2) {
                    button2.setTextColor(highlight_color);
                    button2.setBackgroundColor(Color.TRANSPARENT);
                    select_number = 0;
                } else {
                    make_button2_selected();
                }
                if (null != onToggleClickListen) {
                    onToggleClickListen.onclick(select_number);
                }
            }
        });
    }

    public void make_button1_selected() {
        select_number = 1;
        button1.setTextColor(getResources().getColor(R.color.md_white_1000));
//        button1.setBackgroundDrawable(getResources().getDrawable(R.drawable.left_round_corner));
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            button1.setBackgroundDrawable(getDrawable(context, R.drawable.left_round_corner));
        } else {
            button1.setBackground(getDrawable(context, R.drawable.left_round_corner));
        }
        Drawable background = button1.getBackground();
        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable) background).getPaint().setColor(highlight_color);
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable) background).setColor(highlight_color);
            ((GradientDrawable) background).setStroke(2, highlight_color);
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable) background).setColor(highlight_color);
        }
        button2.setTextColor(highlight_color);
        button2.setBackgroundColor(Color.TRANSPARENT);
    }

    public void make_button2_selected() {
        select_number = 2;
        button2.setTextColor(getResources().getColor(R.color.md_white_1000));
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            button2.setBackgroundDrawable(getDrawable(context, R.drawable.right_round_corner));
        } else {
            button2.setBackground(getDrawable(context, R.drawable.right_round_corner));
        }
        Drawable background = button2.getBackground();
        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable) background).getPaint().setColor(highlight_color);
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable) background).setColor(highlight_color);
            ((GradientDrawable) background).setStroke(2, highlight_color);
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable) background).setColor(highlight_color);
        }
        button1.setTextColor(highlight_color);
        button1.setBackgroundColor(Color.TRANSPARENT);
    }

    public static final Drawable getDrawable(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 21) {
            return ContextCompat.getDrawable(context, id);
        } else {
            return context.getResources().getDrawable(id);
        }
    }
}
