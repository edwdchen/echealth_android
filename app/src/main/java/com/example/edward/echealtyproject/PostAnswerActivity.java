package com.example.edward.echealtyproject;

import android.support.v7.app.AppCompatActivity;

import java.util.HashMap;

/**
 * Created by edward on 12/15/2015.
 */
public class PostAnswerActivity extends AppCompatActivity implements PostAnswerInterface{
    @Override
    public void post_answer(String answer_string, String item_id, OnPostAnswerInterface onPostAnswerInterface) {

    }

    @Override
    public void show_or_hide_next_level_btn(boolean is_show, String level2_record_id, String level3_title,String checklist_choice) {

    }

    @Override
    public void reload() {

    }

    @Override
    public void store_temp_record_answer(int question_list_position, Object answer_object) {

    }

    @Override
    public HashMap<String, Object> get_question(int pos) {
        return null;
    }

    @Override
    public HashMap<String, Object> get_answer(int pos) {
        return null;
    }


}
